﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    internal class VentilationModeAdapter : BaseAdapter<VentilationMode>
    {
        private Activity context;
        private List<VentilationMode> ventilationModeList;

        public VentilationModeAdapter(Activity activity, List<VentilationMode> ventilationModeList)
        {
            this.context = activity;
            this.ventilationModeList = ventilationModeList;
        }

        public override VentilationMode this[int position] => ventilationModeList[position];

        public override int Count => ventilationModeList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = ventilationModeList[position].ToString();

			return view;
        }
    }
}