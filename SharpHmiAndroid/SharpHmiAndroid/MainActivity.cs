﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using System.Timers;
using Android.Content;
using Android.Widget;
using Android.Text;
using Android.Support.V7.Preferences;
using System;
using Android;
using Android.Support.V4.App;
using Android.Content.PM;
using HmiApiLib.Controllers.UI.IncomingRequests;
using HmiApiLib.Common.Enums;
using Android.Speech.Tts;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using Android.Runtime;
using System.Collections.Generic;
using HmiApiLib.Common.Structs;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using HmiApiLib.Types;
using HmiApiLib.Utility;
using HmiApiLib;
using HmiApiLib.Handshaking;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using static HmiApiLib.InitialConnectionCommandConfig;
using Java.IO;

namespace SharpHmiAndroid
{
    [Activity(Label = "SharpHmiAndroid", MainLauncher = true, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    public class MainActivity : AppCompatActivity, AppUiCallback, ActivityCompat.IOnRequestPermissionsResultCallback, TextToSpeech.IOnInitListener
    {
        NavigationView navigationView;
        DrawerLayout drawer;
        private AppSetting appSetting;
        private static string CONSOLE_FRAGMENT_TAG = "console_frag";
        private static string MAIN_FRAGMENT_TAG = "main_frag";
        static string HMI_FULL_FRAGMENT_TAG = "hmi_full_frag";
        static string HMI_OPTIONS_MENU_FRAGMENT_TAG = "hmi_options_menu_frag";
        static string PERFORM_INTERACTION_FRAGMENT_TAG = "perfrom_interaction_frag";
        public const int REQUEST_STORAGE = 10;
        private static string APP_ID = "00000000000000000000000000000000";
        AppInstanceManager.SelectionMode selectedMode = AppInstanceManager.SelectionMode.NONE;

        Handler mHandler;
        Action action;

        private string RegisterButtonSubscription = ComponentPrefix.Buttons + "." + FunctionType.OnButtonSubscription;
        private string RegisterOnAppRegister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppRegistered;
        private string RegisterOnAppUnRegister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppUnregistered;
        private string RegisterPutfile = ComponentPrefix.BasicCommunication + "." + FunctionType.OnPutFile;
        private string RegisterVideoStreaming = ComponentPrefix.Navigation + "." + FunctionType.OnVideoDataStreaming;
        ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam = null;

        TextToSpeech textToSpeech;
        List<string> speechList = new List<string>();

        string[] resultCode = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
        string[] speechCapabilities = Enum.GetNames(typeof(SpeechCapabilities));
        string[] prerecordedSpeech = Enum.GetNames(typeof(PrerecordedSpeech));
        string[] languages = Enum.GetNames(typeof(Language));
        string[] buttonCapabilitiesArray = Enum.GetNames(typeof(ButtonName));
        string[] vehicleDataNotificationStatusArray = Enum.GetNames(typeof(VehicleDataNotificationStatus));
        string[] ECallConfirmationStatusArray = Enum.GetNames(typeof(ECallConfirmationStatus));
        string[] AmbientLightStatusArray = Enum.GetNames(typeof(AmbientLightStatus));
        String[] DeviceLevelStatusArray = Enum.GetNames(typeof(DeviceLevelStatus));
        String[] PrimaryAudioSourceArray = Enum.GetNames(typeof(PrimaryAudioSource));
        String[] IgnitionStableStatusArray = Enum.GetNames(typeof(IgnitionStableStatus));
        String[] IgnitionStatusArray = Enum.GetNames(typeof(IgnitionStatus));
        String[] VehicleDataEventStatusArray = Enum.GetNames(typeof(VehicleDataEventStatus));
        String[] WarningLightStatusArray = Enum.GetNames(typeof(WarningLightStatus));
        String[] SingleTireStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] CompassDirectionArray = Enum.GetNames(typeof(CompassDirection));
        String[] DimensionArray = Enum.GetNames(typeof(Dimension));
        String[] EmergencyEventTypeArray = Enum.GetNames(typeof(EmergencyEventType));
        String[] FuelCutoffStatusArray = Enum.GetNames(typeof(FuelCutoffStatus));
        String[] ComponentVolumeStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] PRNDLArray = Enum.GetNames(typeof(PRNDL));
        String[] WiperStatusArray = Enum.GetNames(typeof(WiperStatus));
        String[] VehicleDataStatusArray = Enum.GetNames(typeof(VehicleDataStatus));
        String[] PowerModeQualificationStatusArray = Enum.GetNames(typeof(PowerModeQualificationStatus));
        String[] CarModeStatusArray = Enum.GetNames(typeof(CarModeStatus));
        String[] PowerModeStatusArray = Enum.GetNames(typeof(PowerModeStatus));
        String[] RCDefrostZoneArray = Enum.GetNames(typeof(DefrostZone));
        String[] RCVentilationModeArray = Enum.GetNames(typeof(VentilationMode));
        string[] buttonNames = Enum.GetNames(typeof(ButtonName));
        string[] electronicParkBrakeStatusArray = Enum.GetNames(typeof(ElectronicParkBrakeStatus));

        AppInstanceManager theInstance;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.splash_screen);
            theInstance = AppInstanceManager.Instance;
            if ((AppInstanceManager.bRecycled == false) || (theInstance.getAppSetting() == null))
            { //Newly launched application.
                this.appSetting = new AppSetting(this);
                theInstance.setAppSetting(this.appSetting);
                CheckForUpdates();
            }
            else
            {
                this.appSetting = theInstance.getAppSetting();
            }
            theInstance.setAppUiCallback(this);
            if (appSetting != null)
            {
                selectedMode = appSetting.getSelectedMode();
            }
            if (!AppUtils.checkPermission(ApplicationContext, Manifest.Permission.WriteExternalStorage))
            {
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.WriteExternalStorage },
                                                  REQUEST_STORAGE);
            }
            else
            {
                selectMode();
            }
        }

        private void CheckForUpdates()
        {
            UpdateManager.Register(this, APP_ID);
        }

        private void UnregisterManagers()
        {
            UpdateManager.Unregister();
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, APP_ID);
        }

        protected override void OnPause()
        {
            base.OnPause();
            UnregisterManagers();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            UnregisterManagers();
        }

        void selectMode()
        {
            if (selectedMode == AppInstanceManager.SelectionMode.NONE)
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
                SetContentView(Resource.Layout.splash_selection_screen);
                Button debugModeButton = (Button)FindViewById(Resource.Id.splash_debug_view);
                Button basicModeButton = (Button)FindViewById(Resource.Id.splash_basic_view);

                debugModeButton.Click += (sender, e) =>
                {
                    selectedMode = AppInstanceManager.SelectionMode.DEBUG_MODE;
                    mainActivityInitialization();
                    prefs.Edit().PutInt(Const.PREFS_KEY_SELECTION_MODE, (int)selectedMode).Commit();
                };

                basicModeButton.Click += (sender, e) =>
                {
                    Toast.MakeText(this, "HMI Sample Coming Soon.", ToastLength.Long).Show();
                    return;
                    /*selectedMode = AppInstanceManager.SelectionMode.BASIC_MODE;
					mainActivityInitialization();
                    prefs.Edit().PutInt(Const.PREFS_KEY_SELECTION_MODE, (int)selectedMode).Apply();*/
                };
            }
            else
            {
                mainActivityInitialization();
            }
        }

        public void mainActivityInitialization()
        {
            if (selectedMode == AppInstanceManager.SelectionMode.BASIC_MODE)
            {
                SetContentView(Resource.Layout.main_basic);
                navigationView = FindViewById<NavigationView>(Resource.Id.nav_view_basic);
            }
            else if (selectedMode == AppInstanceManager.SelectionMode.DEBUG_MODE)
            {
                SetContentView(Resource.Layout.Main);
                navigationView = FindViewById<NavigationView>(Resource.Id.nav_view_debug);
            }

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            //			SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            Android.Support.V7.App.ActionBarDrawerToggle _actionBarDrawerToggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open,
            Resource.String.navigation_drawer_close);

            drawer.SetDrawerListener(_actionBarDrawerToggle);

            navigationView.NavigationItemSelected += OnNavigationItemSelected;

            MetricsManager.Register(Application, APP_ID);

            //Register to Feedback Manager.
            FeedbackManager.Register(this, APP_ID, null);
            if (SdlService.instance != null)
            {
                setConsoleFragment();
            }
            else
            {
                settingsDialog();
            }
        }

        public void showFeedbackActivity()
        {
            try
            {
                FeedbackManager.ShowFeedbackActivity(ApplicationContext);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Feedback Activity could not be displayed ", ToastLength.Long).Show();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            switch (requestCode)
            {
                case REQUEST_STORAGE:
                    {
                        if (grantResults.Length > 0 && (grantResults[0] == (int)Permission.Granted))
                        {
                            selectMode();
                        }
                        else
                        {
                            Toast.MakeText(this, "Permission Denied.", ToastLength.Long).Show();
                        }
                    }
                    break;
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.main, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        private void OnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var menuItem = e.MenuItem;
            menuItem.SetChecked(!menuItem.IsChecked);
            drawer.CloseDrawers();

            switch (menuItem.ItemId)
            {
                case Resource.Id.consoleLogs:
                    clearAllBackStackFragments();
                    Toast.MakeText(Application.Context, "Console Logs selected", ToastLength.Long).Show();
                    break;

                case Resource.Id.findApps:
                    setMainFragment();
                    Toast.MakeText(Application.Context, "Find Apps selected", ToastLength.Long).Show();
                    break;

                case Resource.Id.feedbackActivity:
                    showFeedbackActivity();
                    Toast.MakeText(Application.Context, "App Feedback selected", ToastLength.Long).Show();
                    break;

                case Resource.Id.nav_exit:
                    exitApp();
                    Toast.MakeText(Application.Context, "Exit App selected", ToastLength.Long).Show();
                    break;

                default:
                    Toast.MakeText(Application.Context, "Something is Wrong", ToastLength.Long).Show();
                    break;

            }
        }

        /** Closes the activity and stops the proxy service. */
        private void exitApp()
        {
            Finish();

            var timer = new Timer();
            //What to do when the time elapses
            timer.Elapsed += (sender, args) => ExitAppCallback();
            //How often (5 sec)
            timer.Interval = 50;
            //Start it!
            timer.Enabled = true;
        }

        private void ExitAppCallback()
        {
            Process.KillProcess(Process.MyPid());
        }

        /*Method for clearing out all backstack entries for fragments.*/
        public void clearAllBackStackFragments()
        {
            Android.App.FragmentManager fm = this.FragmentManager;
            fm.PopBackStackImmediate();
        }

        public void setMainFragment()
        {
            MainFragment mainFragment = getMainFragment();

            Android.App.FragmentManager fragmentManager = this.FragmentManager;

            if (mainFragment == null)
            {
                mainFragment = new MainFragment();
                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, mainFragment, MAIN_FRAGMENT_TAG).AddToBackStack(null).Commit();
                fragmentManager.ExecutePendingTransactions();
                this.SetTitle(Resource.String.app_name);
            }
            SupportActionBar.Title = "Apps";
        }

        public void setConsoleFragment()
        {

            ConsoleFragment consoleFragment = getConsoleFragment();

            Android.App.FragmentManager fragmentManager = this.FragmentManager;

            if (consoleFragment == null)
            {
                consoleFragment = new ConsoleFragment();
                fragmentManager.BeginTransaction()
                               .Add(Resource.Id.frame_container, consoleFragment, CONSOLE_FRAGMENT_TAG).CommitAllowingStateLoss();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.settings:
                    settingsDialog();
                    return true;

                case Resource.Id.jsonParser:
                    jsonParser();
                    return true;

                case Resource.Id.appVersion:
                    showAppVersion();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private EditText txtLocalFileName;
        private string requiredPermission = null;

        public void jsonParser()
        {
            Android.Support.V7.App.AlertDialog.Builder showMoreDialogBuilder;
            Android.Support.V7.App.AlertDialog dlg;

            showMoreDialogBuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)ApplicationContext
                .GetSystemService(Context.LayoutInflaterService);
            View view = inflater.Inflate(Resource.Layout.jsonparser,
                    null);

            showMoreDialogBuilder.SetView(view);

            txtLocalFileName = (EditText)view.FindViewById(Resource.Id.jsonparser_local_file);

            TextView txtFileName = (TextView)view.FindViewById(Resource.Id.jsonparser_local_file_name);

            txtLocalFileName.TextChanged += (sender, e) =>
            {
                File tmpFile = new File(txtLocalFileName.Text);
                if (tmpFile.IsFile && tmpFile.Exists())
                {
                    string tmpFileName = tmpFile.Name;
                    txtFileName.Text = tmpFileName;
                }
                else
                {
                    txtFileName.Text = Resources.GetText(Resource.String.jsonparser_file_default);
                }
            };

            Button btnChooseFile = (Button)view.FindViewById(Resource.Id.jsonparser_select_file_button);

            btnChooseFile.Click += delegate
            {
                requiredPermission = Manifest.Permission.ReadExternalStorage;
                if (AppUtils.checkPermission(this, requiredPermission))
                {
                    Intent intent = new Intent(ApplicationContext, typeof(FileChooserActivity));
                    StartActivityForResult(intent, Const.REQUEST_FILE_CHOOSER);
                }
            };

            ImageView delay_minus = (ImageView)view.FindViewById(Resource.Id.jsonparser_delay_minus);
            ImageView delay_plus = (ImageView)view.FindViewById(Resource.Id.jsonparser_delay_plus);
            EditText delay_timer = (EditText)view.FindViewById(Resource.Id.jsonparser_delay_timer);

            delay_minus.Click += delegate
            {

                double timer = double.Parse(delay_timer.EditableText.ToString());
                if (timer > 0.0)
                {
                    timer = timer - 0.1;
                    delay_timer.Text = "" + timer;
                }
            };

            delay_plus.Click += delegate
            {
                double timer = double.Parse(delay_timer.EditableText.ToString());
                timer = timer + 0.1;
                delay_timer.Text = "" + timer;
            };

            delay_timer.TextChanged += (sender, e) =>
            {
                if (double.Parse(delay_timer.Text) <= 0.0)
                {
                    delay_minus.Enabled = false;
                }
                else
                {
                    delay_minus.Enabled = true;
                }
            };

            showMoreDialogBuilder
            .SetTitle("Select JSON File")
            .SetCancelable(false)
            .SetNegativeButton("Cancel", (senderAlert, args) =>
            {

            })
			.SetNeutralButton("Stop Tx", (senderAlert, args) =>
			{
                JsonParser.Instance.stopAutoParseJSONFile();
			})
           .SetPositiveButton("Auto TX", (senderAlert, args) =>
                {
                    if ((null != txtLocalFileName.Text && (txtLocalFileName.Text.Length > 0)))
                    {
                        File initialFile = new File(txtLocalFileName.Text);
                        System.IO.FileStream targetStream = new System.IO.FileStream(txtLocalFileName.Text, System.IO.FileMode.Open);
                        JsonParser.Instance.startAutoParseJSONFile(targetStream, double.Parse(delay_timer.Text));
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                    .SetTitle("Empty File")
                                    .SetMessage("File is empty, please choose another file.")
                                   .SetPositiveButton("OK", (sen, arg) => { }).Show();
                        txtLocalFileName.Text = "";
                    }
                });

            dlg = showMoreDialogBuilder.Create();
            dlg.Show();

            Button jsonPreview = dlg.GetButton((int)DialogButtonType.Neutral);
            jsonPreview.Enabled = true;

        }

        public void showAppVersion()
        {
            String appVersion = "App Ver: ";
            try
            {
                appVersion += PackageManager.GetPackageInfo(PackageName, 0).VersionName;
            }
            catch (Exception e)
            {
                appVersion += "Unknown";
            }

            String libVersion = "Proxy Lib Ver: ";

            if (AppInstanceManager.Instance.getProxyVersionInfo() != null)
            {
                libVersion += AppInstanceManager.Instance.getProxyVersionInfo();
            }
            else
            {
                libVersion += "Info not available";
            }

            new Android.Support.V7.App.AlertDialog.Builder(this).
                       SetTitle("SharpHmi Version").
                       SetMessage(appVersion + "\n\n" + libVersion + "\n")
                       .SetNeutralButton(Android.Resource.String.Ok, (senderAlert, args) =>
            {
            }).Create().Show();
        }

        public void settingsDialog()
        {
            Android.Support.V7.App.AlertDialog.Builder builder;
            Android.Support.V7.App.AlertDialog dlg;

            appSetting = AppInstanceManager.Instance.getAppSetting();

            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View layout = inflater.Inflate(Resource.Layout.setting_option, null);

            EditText ipAddressEditText = layout
                .FindViewById(Resource.Id.selectprotocol_ipAddr) as EditText;
            EditText tcpPortEditText = (EditText)layout
                .FindViewById(Resource.Id.selectprotocol_tcpPort);

            CheckBox registerUi = (CheckBox)layout.FindViewById(Resource.Id.register_ui);
            CheckBox registerBasicCommunication = (CheckBox)layout.FindViewById(Resource.Id.register_basic_communication);
            CheckBox registerButtons = (CheckBox)layout.FindViewById(Resource.Id.register_buttons);
            CheckBox registerVr = (CheckBox)layout.FindViewById(Resource.Id.register_vr);
            CheckBox registerTts = (CheckBox)layout.FindViewById(Resource.Id.register_tts);
            CheckBox registerNavigation = (CheckBox)layout.FindViewById(Resource.Id.register_navigation);
            CheckBox registerVehicleInfo = (CheckBox)layout.FindViewById(Resource.Id.register_vehicle_info);
            CheckBox registerRc = (CheckBox)layout.FindViewById(Resource.Id.register_rc);

            CheckBox buildBCOnReady = (CheckBox)layout.FindViewById(Resource.Id.build_bc_on_ready);
            CheckBox subscribeButtonSubscription = (CheckBox)layout.FindViewById(Resource.Id.subscribe_button_subscription);
            CheckBox subscribeAppRegister = (CheckBox)layout.FindViewById(Resource.Id.subscribe_app_register);
            CheckBox subscribeAppUnregister = (CheckBox)layout.FindViewById(Resource.Id.subscribe_app_unregister);
            CheckBox subscribePutfile = (CheckBox)layout.FindViewById(Resource.Id.subscribe_putfile);
            CheckBox subscribeVideoStreaming = (CheckBox)layout.FindViewById(Resource.Id.subscribe_video_streaming);

            CheckBox chkResponseBCMixAudio = (CheckBox)layout.FindViewById(Resource.Id.response_bc_mix_audio_support_chk);
            CheckBox chkResponseButtonsGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_button_get_capabilities_chk);
            CheckBox chkResponseNavigationIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_navigation_is_ready_chk);
            CheckBox chkResponseRCGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_rc_get_capabilities_chk);
            CheckBox chkResponseRCIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_rc_is_ready_chk);
            CheckBox chkResponseTTSGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_capabilities_chk);
            CheckBox chkResponseTTSGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_language_chk);
            CheckBox chkResponseTTSGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_supported_language_chk);
            CheckBox chkResponseTTSIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_tts_is_ready_chk);
            CheckBox chkResponseUIGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_capabilities_chk);
            CheckBox chkResponseUIGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_language_chk);
            CheckBox chkResponseUIGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_supported_language_chk);
            CheckBox chkResponseUIIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_ui_is_ready_chk);
            CheckBox chkResponseVIGetVehicleData = (CheckBox)layout.FindViewById(Resource.Id.response_vi_get_vehicle_data_chk);
            CheckBox chkResponseVIGetVehicleType = (CheckBox)layout.FindViewById(Resource.Id.response_vi_get_vehicle_type_chk);
            CheckBox chkResponseVIIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_vi_is_ready_chk);
            CheckBox chkResponseVRGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_capabilities_chk);
            CheckBox chkResponseVRGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_language_chk);
            CheckBox chkResponseVRGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_supported_language_chk);
            CheckBox chkResponseVRIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_vr_is_ready_chk);
            CheckBox chkResponseBCGetSystemInfo = (CheckBox)layout.FindViewById(Resource.Id.response_bc_get_system_info_chk);


            Button btnResponseBCMixAudio = (Button)layout.FindViewById(Resource.Id.response_bc_mix_audio_support_button);
            Button btnResponseButtonsGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_button_get_capabilities_button);
            Button btnResponseNavigationIsReady = (Button)layout.FindViewById(Resource.Id.response_navigation_is_ready_button);
            Button btnResponseRCGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_rc_get_capabilities_button);
            Button btnResponseRCIsReady = (Button)layout.FindViewById(Resource.Id.response_rc_is_ready_button);
            Button btnResponseTTSGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_tts_get_capabilities_button);
            Button btnResponseTTSGetLanguage = (Button)layout.FindViewById(Resource.Id.response_tts_get_language_button);
            Button btnResponseTTSGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_tts_get_supported_language_button);
            Button btnResponseTTSIsReady = (Button)layout.FindViewById(Resource.Id.response_tts_is_ready_button);
            Button btnResponseUIGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_ui_get_capabilities_button);
            Button btnResponseUIGetLanguage = (Button)layout.FindViewById(Resource.Id.response_ui_get_language_button);
            Button btnResponseUIGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_ui_get_supported_language_button);
            Button btnResponseUIIsReady = (Button)layout.FindViewById(Resource.Id.response_ui_is_ready_button);
            Button btnResponseVIGetVehicleData = (Button)layout.FindViewById(Resource.Id.response_vi_get_vehicle_data_button);
            Button btnResponseVIGetVehicleType = (Button)layout.FindViewById(Resource.Id.response_vi_get_vehicle_type_button);
            Button btnResponseVIIsReady = (Button)layout.FindViewById(Resource.Id.response_vi_is_ready_button);
            Button btnResponseVRGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_vr_get_capabilities_button);
            Button btnResponseVRGetLanguage = (Button)layout.FindViewById(Resource.Id.response_vr_get_language_button);
            Button btnResponseVRGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_vr_get_supported_language_button);
            Button btnResponseVRIsReady = (Button)layout.FindViewById(Resource.Id.response_vr_is_ready_button);
            Button btnResponseBCGetSystemInfo = (Button)layout.FindViewById(Resource.Id.response_bc_get_system_info_button);

            chkResponseBCMixAudio.CheckedChange += (sender, e) => btnResponseBCMixAudio.Enabled = e.IsChecked;
            chkResponseButtonsGetCapabilites.CheckedChange += (sender, e) => btnResponseButtonsGetCapabilites.Enabled = e.IsChecked;
            chkResponseNavigationIsReady.CheckedChange += (sender, e) => btnResponseNavigationIsReady.Enabled = e.IsChecked;
            chkResponseRCGetCapabilites.CheckedChange += (sender, e) => btnResponseRCGetCapabilites.Enabled = e.IsChecked;
            chkResponseRCIsReady.CheckedChange += (sender, e) => btnResponseRCIsReady.Enabled = e.IsChecked;
            chkResponseTTSGetCapabilites.CheckedChange += (sender, e) => btnResponseTTSGetCapabilites.Enabled = e.IsChecked;
            chkResponseTTSGetLanguage.CheckedChange += (sender, e) => btnResponseTTSGetLanguage.Enabled = e.IsChecked;
            chkResponseTTSGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseTTSGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseTTSIsReady.CheckedChange += (sender, e) => btnResponseTTSIsReady.Enabled = e.IsChecked;
            chkResponseUIGetCapabilites.CheckedChange += (sender, e) => btnResponseUIGetCapabilites.Enabled = e.IsChecked;
            chkResponseUIGetLanguage.CheckedChange += (sender, e) => btnResponseUIGetLanguage.Enabled = e.IsChecked;
            chkResponseUIGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseUIGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseUIIsReady.CheckedChange += (sender, e) => btnResponseUIIsReady.Enabled = e.IsChecked;
            chkResponseVIGetVehicleData.CheckedChange += (sender, e) => btnResponseVIGetVehicleData.Enabled = e.IsChecked;
            chkResponseVIGetVehicleType.CheckedChange += (sender, e) => btnResponseVIGetVehicleType.Enabled = e.IsChecked;
            chkResponseVIIsReady.CheckedChange += (sender, e) => btnResponseVIIsReady.Enabled = e.IsChecked;
            chkResponseVRGetCapabilites.CheckedChange += (sender, e) => btnResponseVRGetCapabilites.Enabled = e.IsChecked;
            chkResponseVRGetLanguage.CheckedChange += (sender, e) => btnResponseVRGetLanguage.Enabled = e.IsChecked;
            chkResponseVRGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseVRGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseVRIsReady.CheckedChange += (sender, e) => btnResponseVRIsReady.Enabled = e.IsChecked;
            chkResponseBCGetSystemInfo.CheckedChange += (sender, e) => btnResponseBCGetSystemInfo.Enabled = e.IsChecked;

            chkResponseBCMixAudio.Checked = appSetting.getBCMixAudioSupport();
            chkResponseButtonsGetCapabilites.Checked = appSetting.getButtonGetCapabilities();
            chkResponseNavigationIsReady.Checked = appSetting.getNavigationIsReady();
            chkResponseRCGetCapabilites.Checked = appSetting.getRCGetCapabilities();
            chkResponseRCIsReady.Checked = appSetting.getRCIsReady();
            chkResponseTTSGetCapabilites.Checked = appSetting.getTTSGetCapabilities();
            chkResponseTTSGetLanguage.Checked = appSetting.getTTSGetLanguage();
            chkResponseTTSGetSupportedLanguage.Checked = appSetting.getTTSGetSupportedLanguage();
            chkResponseTTSIsReady.Checked = appSetting.getTTSIsReady();
            chkResponseUIGetCapabilites.Checked = appSetting.getUIGetCapabilities();
            chkResponseUIGetLanguage.Checked = appSetting.getUIGetLanguage();
            chkResponseUIGetSupportedLanguage.Checked = appSetting.getUIGetSupportedLanguage();
            chkResponseUIIsReady.Checked = appSetting.getUIIsReady();
            chkResponseVIGetVehicleData.Checked = appSetting.getVIGetVehicleData();
            chkResponseVIGetVehicleType.Checked = appSetting.getVIGetVehicleType();
            chkResponseVIIsReady.Checked = appSetting.getVIIsReady();
            chkResponseVRGetCapabilites.Checked = appSetting.getVRGetCapabilities();
            chkResponseVRGetLanguage.Checked = appSetting.getVRGetLanguage();
            chkResponseVRGetSupportedLanguage.Checked = appSetting.getVRGetSupportedLanguage();
            chkResponseVRIsReady.Checked = appSetting.getVRIsReady();
            chkResponseBCGetSystemInfo.Checked = appSetting.getBCGetSystemInfo();

            btnResponseBCMixAudio.Click += (object sender, EventArgs e) => CreateBCResponseMixingAudioSupported();
            btnResponseButtonsGetCapabilites.Click += (object sender, EventArgs e) => CreateButtonsGetCapabilities();
            btnResponseNavigationIsReady.Click += (object sender, EventArgs e) => CreateNavigationResponseIsReady();
            btnResponseRCGetCapabilites.Click += (object sender, EventArgs e) => CreateRCResponseGetCapabilities();
            btnResponseRCIsReady.Click += (object sender, EventArgs e) => CreateRCResponseIsReady();
            btnResponseTTSGetCapabilites.Click += (object sender, EventArgs e) => CreateTTSResponseGetCapabilities();
            btnResponseTTSGetLanguage.Click += (object sender, EventArgs e) => CreateTTSResponseGetLanguage();
            btnResponseTTSGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateTTSResponseGetSupportedLanguages();
            btnResponseTTSIsReady.Click += (object sender, EventArgs e) => CreateTTSResponseIsReady();
            btnResponseUIGetCapabilites.Click += (object sender, EventArgs e) => CreateUIResponseGetCapabilities();
            btnResponseUIGetLanguage.Click += (object sender, EventArgs e) => CreateUIResponseGetLanguage();
            btnResponseUIGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateUIResponseGetSupportedLanguages();
            btnResponseUIIsReady.Click += (object sender, EventArgs e) => CreateUIResponseIsReady();
            btnResponseVIGetVehicleData.Click += (object sender, EventArgs e) => CreateVIResponseGetVehicleData();
            btnResponseVIGetVehicleType.Click += (object sender, EventArgs e) => CreateVIResponseGetVehicleType();
            btnResponseVIIsReady.Click += (object sender, EventArgs e) => CreateVIResponseIsReady();
            btnResponseVRGetCapabilites.Click += (object sender, EventArgs e) => CreateVRResponseGetCapabilities();
            btnResponseVRGetLanguage.Click += (object sender, EventArgs e) => CreateVRResponseGetLanguage();
            btnResponseVRGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateVRResponseGetSupportedLanguages();
            btnResponseVRIsReady.Click += (object sender, EventArgs e) => CreateVRResponseIsReady();
            btnResponseBCGetSystemInfo.Click += (object sender, EventArgs e) => CreateBCResponseGetSystemInfo();


            string ipAddress = appSetting.getIPAddress();
            string tcpPort = appSetting.getTcpPort();
            InitialConnectionCommandConfig initConfig = appSetting.getInitialConnectionCommandConfig();

            if (initConfig != null)
            {
                buttonCapabilitiesResponseParam = initConfig.getButtonsCapabilitiesResponseParam();
                List<InterfaceType> registerComponents = initConfig.getRegisterComponents();
                if (registerComponents != null)
                {
                    for (int i = 0; i < registerComponents.Count; i++)
                    {
                        switch (registerComponents[i])
                        {
                            case InterfaceType.UI:
                                registerUi.Checked = true;
                                break;
                            case InterfaceType.BasicCommunication:
                                registerBasicCommunication.Checked = true;
                                break;
                            case InterfaceType.Buttons:
                                registerButtons.Checked = true;
                                break;
                            case InterfaceType.VR:
                                registerVr.Checked = true;
                                break;
                            case InterfaceType.TTS:
                                registerTts.Checked = true;
                                break;
                            case InterfaceType.Navigation:
                                registerNavigation.Checked = true;
                                break;
                            case InterfaceType.VehicleInfo:
                                registerVehicleInfo.Checked = true;
                                break;
                            case InterfaceType.RC:
                                registerRc.Checked = true;
                                break;
                        }
                    }
                }

                buildBCOnReady.Checked = initConfig.getIsBcOnReady();
                List<PropertyName> subscribeNotifications = initConfig.getSubscribeNotifications();
                if (subscribeNotifications != null)
                {
                    for (int j = 0; j < subscribeNotifications.Count; j++)
                    {
                        if (subscribeNotifications[j].propertyName.Equals(RegisterButtonSubscription))
                        {
                            subscribeButtonSubscription.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterOnAppRegister))
                        {
                            subscribeAppRegister.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterOnAppUnRegister))
                        {
                            subscribeAppUnregister.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterPutfile))
                        {
                            subscribePutfile.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterVideoStreaming))
                        {
                            subscribeVideoStreaming.Checked = true;
                        }
                    }
                }
            }

            builder = new Android.Support.V7.App.AlertDialog.Builder(this);

            ipAddressEditText.Text = ipAddress;
            tcpPortEditText.Text = tcpPort;

            string htmlString = "<b><u>SharpHmi Settings</u></b>";
            TextView title = new TextView(this);
            title.Gravity = GravityFlags.Center;
            title.Text = Html.FromHtml(htmlString).ToString();
            title.TextSize = 25;

            builder.SetCustomTitle(title);
            builder.SetPositiveButton("OK", (senderAlert, args) =>
            {
                if (selectedMode == AppInstanceManager.SelectionMode.DEBUG_MODE)
                {
                    setConsoleFragment();
                }
                else if (selectedMode == AppInstanceManager.SelectionMode.BASIC_MODE)
                {
                    setMainFragment();
                }

                List<InterfaceType> selectedEnumList = new List<InterfaceType>();
                List<PropertyName> propertyNameList = new List<PropertyName>();
                if (registerUi.Checked)
                {
                    selectedEnumList.Add(InterfaceType.UI);
                }
                if (registerBasicCommunication.Checked)
                {
                    selectedEnumList.Add(InterfaceType.BasicCommunication);
                }
                if (registerButtons.Checked)
                {
                    selectedEnumList.Add(InterfaceType.Buttons);
                }
                if (registerVr.Checked)
                {
                    selectedEnumList.Add(InterfaceType.VR);
                }
                if (registerTts.Checked)
                {
                    selectedEnumList.Add(InterfaceType.TTS);
                }
                if (registerNavigation.Checked)
                {
                    selectedEnumList.Add(InterfaceType.Navigation);
                }
                if (registerVehicleInfo.Checked)
                {
                    selectedEnumList.Add(InterfaceType.VehicleInfo);
                }
                if (registerRc.Checked)
                {
                    selectedEnumList.Add(InterfaceType.RC);
                }
                PropertyName propertyName = new PropertyName();
                if (subscribeButtonSubscription.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.Buttons, FunctionType.OnButtonSubscription);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeAppRegister.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppRegistered);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeAppUnregister.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppUnregistered);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribePutfile.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnPutFile);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeVideoStreaming.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnVideoDataStreaming);
                    propertyNameList.Add(propertyName);
                }
                if (ipAddressEditText.Length() != 0)
                {
                    appSetting.setIPAddress(ipAddressEditText.Text);
                }

                if (tcpPortEditText.Length() != 0)
                {
                    appSetting.setTcpPort(tcpPortEditText.Text);
                }

                InitialConnectionCommandConfig initialConnectionCommandConfig = new InitialConnectionCommandConfig();
                initialConnectionCommandConfig.setRegisterComponents(selectedEnumList);
                initialConnectionCommandConfig.setIsBcOnReady(buildBCOnReady.Checked);
                initialConnectionCommandConfig.setSubscribeNotifications(propertyNameList);
                initialConnectionCommandConfig.setButtonsCapabilitiesResponseParam(buttonCapabilitiesResponseParam);

                appSetting.setInitialConnectionCommandConfig(initialConnectionCommandConfig);

                string tmpIpAddress = appSetting.getIPAddress();
                string tmpTcpPortNumber = appSetting.getTcpPort();

                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);

                String objStr = Newtonsoft.Json.JsonConvert.SerializeObject(initialConnectionCommandConfig);

                prefs.Edit().PutString(Const.PREFS_KEY_TRANSPORT_IP, appSetting.getIPAddress())
                            .PutInt(Const.PREFS_KEY_TRANSPORT_PORT, int.Parse(appSetting.getTcpPort()))
                            .PutString(Const.PREFS_KEY_INITIAL_CONFIG, objStr)
                     .PutBoolean(Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED, chkResponseBCMixAudio.Checked)
                     .PutBoolean(Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES, chkResponseButtonsGetCapabilites.Checked)
                     .PutBoolean(Const.PREFS_KEY_NAVIGATION_IS_READY, chkResponseNavigationIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_RC_GET_CAPABILITIES, chkResponseRCGetCapabilites.Checked)
                     .PutBoolean(Const.PREFS_KEY_RC_IS_READY, chkResponseRCIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_TTS_GET_CAPABILITIES, chkResponseTTSGetCapabilites.Checked)
                     .PutBoolean(Const.PREFS_KEY_TTS_GET_LANGUAGE, chkResponseTTSGetLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE, chkResponseTTSGetSupportedLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_TTS_IS_READY, chkResponseTTSIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_UI_GET_CAPABILITIES, chkResponseUIGetCapabilites.Checked)
                     .PutBoolean(Const.PREFS_KEY_UI_GET_LANGUAGE, chkResponseUIGetLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE, chkResponseUIGetSupportedLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_UI_IS_READY, chkResponseUIIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_DATA, chkResponseVIGetVehicleData.Checked)
                     .PutBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_TYPE, chkResponseVIGetVehicleType.Checked)
                     .PutBoolean(Const.PREFS_KEY_VI_IS_READY, chkResponseVIIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_VR_GET_CAPABILITIES, chkResponseVRGetCapabilites.Checked)
                     .PutBoolean(Const.PREFS_KEY_VR_GET_LANGUAGE, chkResponseVRGetLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE, chkResponseVRGetSupportedLanguage.Checked)
                     .PutBoolean(Const.PREFS_KEY_VR_IS_READY, chkResponseVRIsReady.Checked)
                     .PutBoolean(Const.PREFS_KEY_BC_GET_SYSTEM_INFO, chkResponseBCGetSystemInfo.Checked)
                     .Commit();

                if ((ipAddress != tmpIpAddress) || (tcpPort != tmpTcpPortNumber) || (AppInstanceManager.currentState != AppInstanceManager.ConnectionState.CONNECTED))
                {
                    new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                    {
                        AppInstanceManager.Instance.setupConnection(appSetting.getIPAddress(), int.Parse(appSetting.getTcpPort()), initialConnectionCommandConfig);
                    })).Start();

                }
                else
                {
                    Toast.MakeText(this, "Changes will be effective after New Conection", ToastLength.Long).Show();
                }

                builder.Dispose();
            });

            builder.SetNegativeButton("Cancel", (senderAlert, args) =>
            {
                builder.Dispose();
            });

            builder.SetCancelable(true);
            builder.SetView(layout);
            dlg = builder.Create();
            dlg.Show();
        }

        public ConsoleFragment getConsoleFragment()
        {
            return (ConsoleFragment)FragmentManager.FindFragmentByTag(CONSOLE_FRAGMENT_TAG);
        }

        public MainFragment getMainFragment()
        {
            return (MainFragment)this.FragmentManager.FindFragmentByTag(MAIN_FRAGMENT_TAG);
        }

        public FullHmiFragment getFullHMiFragment()
        {
            return (FullHmiFragment)this.FragmentManager.FindFragmentByTag(HMI_FULL_FRAGMENT_TAG);
        }

        public OptionsMenuFragment getOptionsMenuFragment()
        {
            return (OptionsMenuFragment)this.FragmentManager.FindFragmentByTag(HMI_OPTIONS_MENU_FRAGMENT_TAG);
        }

        public ChoiceListFragment getChoiceListFragment()
        {
            return (ChoiceListFragment)this.FragmentManager.FindFragmentByTag(PERFORM_INTERACTION_FRAGMENT_TAG);
        }

        public void onBcAppRegisteredNotificationCallback(Boolean isNewAppRegistered)
        {
            if (isNewAppRegistered)
            {
                if (getMainFragment() is MainFragmentCallback)
                {
                    getMainFragment().onRefreshCallback();
                }
            }
            else
            {
                if (getFullHMiFragment() != null && getFullHMiFragment().IsVisible)
                {
                    RunOnUiThread(() => RemoveFullFragment());
                }
                else
                {
                    if (getMainFragment() is MainFragmentCallback)
                    {
                        getMainFragment().onRefreshCallback();
                    }
                }
            }
        }

        public void setHmiFragment(int appId)
        {
            AppInstanceManager.Instance.sendOnAppActivatedNotification(appId);

            FullHmiFragment hmiFragment = getFullHMiFragment();

            Android.App.FragmentManager fragmentManager = FragmentManager;

            //if (hmiFragment == null)
            //{
            hmiFragment = new FullHmiFragment();

            Bundle bundle = new Bundle();
            bundle.PutInt(FullHmiFragment.sClickedAppID, appId);
            hmiFragment.Arguments = bundle;

            Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
            fragmentTransaction.Replace(Resource.Id.frame_container, hmiFragment, HMI_FULL_FRAGMENT_TAG).AddToBackStack(null).Commit();
            fragmentManager.ExecutePendingTransactions();
            SetTitle(Resource.String.app_name);
            //}
        }

        public void setOptionsFragment(int appID)
        {
            OptionsMenuFragment optionsMenuFragment = getOptionsMenuFragment();

            Android.App.FragmentManager fragmentManager = FragmentManager;

            if (optionsMenuFragment == null)
            {
                optionsMenuFragment = new OptionsMenuFragment();

                Bundle bundle = new Bundle();
                bundle.PutInt(FullHmiFragment.sClickedAppID, appID);
                optionsMenuFragment.Arguments = bundle;

                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, optionsMenuFragment, HMI_OPTIONS_MENU_FRAGMENT_TAG).AddToBackStack(null).Commit();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        private void RemoveFullFragment()
        {
            Android.App.FragmentManager fragmentManager = FragmentManager;
            fragmentManager.PopBackStack();
        }

        public void refreshOptionsMenu()
        {
            if ((getOptionsMenuFragment() != null) && (getOptionsMenuFragment().IsVisible))
            {
                getOptionsMenuFragment().onRefreshOptionsMenu();
            }
        }

        public void setDownloadedAppIcon()
        {
            if ((getMainFragment() != null) && (getMainFragment().IsVisible))
            {
                getMainFragment().onRefreshCallback();
            }
        }

        public override void OnBackPressed()
        {
            if ((getOptionsMenuFragment() != null) && (getOptionsMenuFragment().IsVisible))
            {
                if (getOptionsMenuFragment().OnBackPressed())
                {
                    base.OnBackPressed();
                }
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public void onUiShowRequestCallback(Show msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiShowRequestCallback(msg);
            }
        }

        public void onUiAlertRequestCallback(Alert msg)
        {
            RunOnUiThread(() => UpdateAlertUI(msg));
        }

        private void UpdateAlertUI(Alert msg)
        {
            Android.Support.V7.App.AlertDialog.Builder alertBuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = LayoutInflater;
            View view = inflater.Inflate(Resource.Layout.custom_alert_dialog, null);
            alertBuilder.SetView(view);

            TextView alertText1 = (TextView)view.FindViewById(Resource.Id.alert_text_1);
            TextView alertText2 = (TextView)view.FindViewById(Resource.Id.alert_text_2);
            TextView alertText3 = (TextView)view.FindViewById(Resource.Id.alert_text_3);

            Button softButton1 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_1);
            Button softButton2 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_2);
            Button softButton3 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_3);
            Button softButton4 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_4);

            if ((msg.getAlertStrings() != null) && (msg.getAlertStrings().Count > 0))
            {
                for (int i = 0; i < msg.getAlertStrings().Count; i++)
                {
                    switch (msg.getAlertStrings()[i].fieldName)
                    {
                        case TextFieldName.alertText1:
                            alertText1.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        case TextFieldName.alertText2:
                            alertText2.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        case TextFieldName.alertText3:
                            alertText3.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        default:
                            break;
                    }
                }
            }

            if ((msg.getSoftButtons() != null) && (msg.getSoftButtons().Count > 0))
            {
                for (int i = 0; i < msg.getSoftButtons().Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            softButton1.Text = msg.getSoftButtons()[i].getText();
                            softButton1.Visibility = ViewStates.Visible;
                            break;
                        case 1:
                            softButton2.Text = msg.getSoftButtons()[i].getText();
                            softButton2.Visibility = ViewStates.Visible;
                            break;
                        case 2:
                            softButton3.Text = msg.getSoftButtons()[i].getText();
                            softButton3.Visibility = ViewStates.Visible;
                            break;
                        case 3:
                            softButton4.Text = msg.getSoftButtons()[i].getText();
                            softButton4.Visibility = ViewStates.Visible;
                            break;
                        default:
                            break;
                    }
                }
            }

            alertBuilder.SetPositiveButton("ok", (senderAlert, args) =>
            {
                alertBuilder.Dispose();
            });
            Android.Support.V7.App.AlertDialog ad = alertBuilder.Create();
            ad.Show();

            int? totalDuration = (int)msg.getDuration();
            if (totalDuration != null)
            {
                mHandler = new Handler(Looper.MainLooper);
                action = delegate
                {
                    ad.Cancel();
                };
                if (null != mHandler)
                    mHandler.PostDelayed(action, (long)totalDuration);
            }
        }

        public void onUiScrollableMessageRequestCallback(ScrollableMessage msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiScrollableMessageRequestCallback(msg);
            }
        }

        public void onUiPerformInteractionRequestCallback(PerformInteraction msg)
        {
            if ((getChoiceListFragment() != null) && (getChoiceListFragment().IsVisible))
            {
                getChoiceListFragment().onUiPerformInteractionRequestCallback(msg);
            }
            else
            {
                ChoiceListFragment choiceListFrag = getChoiceListFragment();

                Android.App.FragmentManager fragmentManager = FragmentManager;

                choiceListFrag = new ChoiceListFragment(msg);

                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, choiceListFrag, PERFORM_INTERACTION_FRAGMENT_TAG).AddToBackStack(null).Commit();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        public void onUiSetMediaClockTimerRequestCallback(SetMediaClockTimer msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiSetMediaClockTimerRequestCallback(msg);
            }
        }

        public void OnButtonSubscriptionNotificationCallback(HmiApiLib.Controllers.Buttons.IncomingNotifications.OnButtonSubscription msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().OnButtonSubscriptionNotificationCallback(msg);
            }
        }

        public void onTtsSpeakRequestCallback(Speak msg)
        {
            if ((msg.getTtsChunkList() != null) && (msg.getTtsChunkList().Count > 0))
            {
                foreach (TTSChunk item in msg.getTtsChunkList())
                {
                    if (item.type == SpeechCapabilities.TEXT)
                    {
                        speechList.Add(item.text);
                    }
                }
            }
            RunOnUiThread(() => HandleSpeakRequest());
        }

        private void HandleSpeakRequest()
        {
            textToSpeech = new TextToSpeech(this, this);
        }

        public void OnInit([GeneratedEnum] OperationResult status)
        {
            if (status != OperationResult.Error)
            {
                textToSpeech.SetLanguage(Java.Util.Locale.Us);
                for (int i = 0; i < speechList.Count; i++)
                {
                    textToSpeech.Speak(speechList[i], QueueMode.Add, null, null);
                }
                speechList.Clear();
            }
        }

        public void onUiSliderRequestCallback(Slider msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiSliderRequestCallback(msg);
            }
        }

        public void CreateBCResponseMixingAudioSupported()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox checkBoxAttenuatedSupported = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            var adapter1 = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter1;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAttenuatedSupported.Checked = (bool)tmpObj.getAttenuatedSupported();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAttenuatedSupported.Text = "AttenuatedSupported";

            rsltCodeCheckBox.Text = "Result Code";

            rpcAlertDialog.SetTitle("BC.MixingAudioSupported");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationMixingAudioSupportedResponse(BuildRpc.getNextId(), checkBoxAttenuatedSupported.Checked, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter1.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateNavigationResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = (View)inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("Nav.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCode.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.Navigation, checkBoxAvailable.Checked, selectedResultCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.tts_get_capabilities, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("TTS.GetCapabilities");

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.speech_capabilities_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.speech_capabilities_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            bool[] speechCapabilitiesBoolArray = new bool[speechCapabilities.Length];

            CheckBox speechCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.speech_capabilities_cb);
            Button speechCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.speech_capabilities_btn);
            ListView ListViewSpeechCapabilities = (ListView)rpcView.FindViewById(Resource.Id.speech_capabilities_lv);

            List<SpeechCapabilities> speechCapabilitiesList = new List<SpeechCapabilities>();
            var speechCapabilitiesListAdapter = new SpeechCapabilitiesAdapter(this, speechCapabilitiesList);
            ListViewSpeechCapabilities.Adapter = speechCapabilitiesListAdapter;

            speechCapabilitiesCheckBox.CheckedChange += (s, e) =>
            {
                ListViewSpeechCapabilities.Enabled = e.IsChecked;
                speechCapabilitiesButton.Enabled = e.IsChecked;
            };

            speechCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder speechCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                speechCapabilitiesAlertDialog.SetTitle("SpeechCapabilities");

                speechCapabilitiesAlertDialog.SetMultiChoiceItems(speechCapabilities, speechCapabilitiesBoolArray, (sender, e) => speechCapabilitiesBoolArray[e.Which] = e.IsChecked);
                speechCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    speechCapabilitiesAlertDialog.Dispose();
                });

                speechCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    speechCapabilitiesList.Clear();
                    for (int i = 0; i < speechCapabilities.Length; i++)
                    {
                        if (speechCapabilitiesBoolArray[i])
                        {
                            speechCapabilitiesList.Add(((SpeechCapabilities)typeof(SpeechCapabilities).GetEnumValues().GetValue(i)));
                        }
                    }
                    speechCapabilitiesListAdapter.NotifyDataSetChanged();
                });
                speechCapabilitiesAlertDialog.Show();
            };

            bool[] prerecordedSpeechBoolArray = new bool[prerecordedSpeech.Length];

            CheckBox prerecordedSpeechCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.prerecorded_speech_cb);
            Button prerecordedSpeechButton = (Button)rpcView.FindViewById(Resource.Id.prerecorded_speech_btn);
            ListView ListViewPrerecordedSpeech = (ListView)rpcView.FindViewById(Resource.Id.prerecorded_speech_lv);

            List<PrerecordedSpeech> prerecordedSpeechList = new List<PrerecordedSpeech>();
            var prerecordedSpeechListAdapter = new PrerecordedSpeechAdapter(this, prerecordedSpeechList);
            ListViewPrerecordedSpeech.Adapter = prerecordedSpeechListAdapter;


            prerecordedSpeechCheckBox.CheckedChange += (s, e) =>
            {
                ListViewPrerecordedSpeech.Enabled = e.IsChecked;
                prerecordedSpeechButton.Enabled = e.IsChecked;
            };

            prerecordedSpeechButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder prerecordedSpeechAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                prerecordedSpeechAlertDialog.SetTitle("PrerecordedSpeech");

                prerecordedSpeechAlertDialog.SetMultiChoiceItems(prerecordedSpeech, prerecordedSpeechBoolArray, (sender, e) => prerecordedSpeechBoolArray[e.Which] = e.IsChecked);
                prerecordedSpeechAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    prerecordedSpeechAlertDialog.Dispose();
                });

                prerecordedSpeechAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    prerecordedSpeechList.Clear();
                    for (int i = 0; i < prerecordedSpeech.Length; i++)
                    {
                        if (prerecordedSpeechBoolArray[i])
                        {
                            prerecordedSpeechList.Add(((PrerecordedSpeech)typeof(PrerecordedSpeech).GetEnumValues().GetValue(i)));
                        }
                    }

                    prerecordedSpeechListAdapter.NotifyDataSetChanged();
                });
                prerecordedSpeechAlertDialog.Show();
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities>(adapter.Context, tmpObj.getMethod());

            if (tmpObj == null) //lets build the default values to populate our UI interface
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (tmpObj != null)
            {
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getSpeechCapabilities() != null)
                {
                    speechCapabilitiesList.AddRange(tmpObj.getSpeechCapabilities());
                    speechCapabilitiesListAdapter.NotifyDataSetChanged();
                }
                if (tmpObj.getPrerecordedSpeechCapabilities() != null)
                {
                    prerecordedSpeechList.AddRange(tmpObj.getPrerecordedSpeechCapabilities());
                    prerecordedSpeechListAdapter.NotifyDataSetChanged();
                }
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<SpeechCapabilities> speechCapList = null;
                List<PrerecordedSpeech> prerecordedSpchList = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                if (speechCapabilitiesCheckBox.Checked)
                    speechCapList = speechCapabilitiesList;

                if (prerecordedSpeechCheckBox.Checked)
                    prerecordedSpchList = prerecordedSpeechList;

                RpcResponse rpcResponse = BuildRpc.buildTTSGetCapabilitiesResponse(BuildRpc.getNextId(), rsltCode, speechCapList, prerecordedSpchList);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("TTS.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            languageCheckBox.Text = "Language";

            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            languageCheckBox.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnLanguage.SetSelection((int)tmpObj.getLanguage());
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? lang = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildTtsGetLanguageResponse(BuildRpc.getNextId(), lang, rsltCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View getSystemInfoRpcView = inflater.Inflate(Resource.Layout.get_support_languages, null);
            rpcAlertDialog.SetView(getSystemInfoRpcView);
            rpcAlertDialog.SetTitle("TTS.GetSupportedLanguages");

            CheckBox ResultCodeCheckBox = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_result_code_cb);
            Spinner spnResultCode = (Spinner)getSystemInfoRpcView.FindViewById(Resource.Id.get_supported_language_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            ResultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            bool[] languagesBoolArray = new bool[languages.Length];

            CheckBox languagesCheckBox = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_language_cb);
            ListView languagesListView = (ListView)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview);
            Button languagesButton = (Button)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview_btn);

            languagesCheckBox.CheckedChange += (s, e) =>
            {
                languagesButton.Enabled = e.IsChecked;
                languagesListView.Enabled = e.IsChecked;
            };

            List<Language> languagesList = new List<Language>();
            var languagesAdapter = new LanguagesAdapter(this, languagesList);
            languagesListView.Adapter = languagesAdapter;

            languagesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder languagesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                languagesAlertDialog.SetTitle("Language");

                languagesAlertDialog.SetMultiChoiceItems(languages, languagesBoolArray, (sender, e) => languagesBoolArray[e.Which] = e.IsChecked);

                languagesAlertDialog.SetNegativeButton("Add", (senderAlert, args) =>
                {
                    languagesList.Clear();
                    for (int i = 0; i < languagesBoolArray.Length; i++)
                    {
                        if (languagesBoolArray[i])
                        {
                            languagesList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    languagesAdapter.NotifyDataSetChanged();
                });

                languagesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    languagesAlertDialog.Dispose();
                });

                languagesAlertDialog.Show();
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getLanguages() != null)
                {
                    languagesList.AddRange(tmpObj.getLanguages());
                    languagesAdapter.NotifyDataSetChanged();
                }
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<Language> langList = null;

                if (ResultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (languagesCheckBox.Checked)
                    langList = languagesList;

                RpcResponse rpcResponse = BuildRpc.buildTTSGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, langList);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("TTS.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = "Available";
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.TTS, checkBoxAvailable.Checked, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });
            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("UI.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            languageCheckBox.Text = "Language";

            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            languageCheckBox.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = !spnResultCode.Enabled;

            HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnLanguage.SetSelection((int)tmpObj.getLanguage());
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? lang = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildUiGetLanguageResponse(BuildRpc.getNextId(), lang, rsltCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });
            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View getSupportedLanguagesRpcView = inflater.Inflate(Resource.Layout.get_support_languages, null);
            rpcAlertDialog.SetView(getSupportedLanguagesRpcView);
            rpcAlertDialog.SetTitle("UI.GetSupportedLanguages");

            CheckBox ResultCodeCheckBox = (CheckBox)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_result_code_cb);
            Spinner spnResultCode = (Spinner)getSupportedLanguagesRpcView.FindViewById(Resource.Id.get_supported_language_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            ResultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            bool[] languagesBoolArray = new bool[languages.Length];

            CheckBox languagesCheckBox = (CheckBox)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_language_cb);
            ListView languagesListView = (ListView)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview);
            Button languagesButton = (Button)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview_btn);

            languagesCheckBox.CheckedChange += (s, e) =>
            {
                languagesButton.Enabled = e.IsChecked;
                languagesListView.Enabled = e.IsChecked;
            };

            List<Language> languagesList = new List<Language>();
            var languagesAdapter = new LanguagesAdapter(this, languagesList);
            languagesListView.Adapter = languagesAdapter;

            languagesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder languagesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                languagesAlertDialog.SetTitle("Language");

                languagesAlertDialog.SetMultiChoiceItems(languages, languagesBoolArray, (sender, e) => languagesBoolArray[e.Which] = e.IsChecked);

                languagesAlertDialog.SetNegativeButton("Add", (senderAlert, args) =>
                {
                    languagesList.Clear();
                    for (int i = 0; i < languagesBoolArray.Length; i++)
                    {
                        if (languagesBoolArray[i])
                        {
                            languagesList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    languagesAdapter.NotifyDataSetChanged();
                });

                languagesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    languagesAlertDialog.Dispose();
                });
                languagesAlertDialog.Show();
            };

            HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getLanguages() != null)
                {
                    languagesList.AddRange(tmpObj.getLanguages());
                    languagesAdapter.NotifyDataSetChanged();
                }
            }

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<Language> langList = null;

                if (ResultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (languagesCheckBox.Checked)
                    langList = languagesList;

                RpcResponse rpcResponse = BuildRpc.buildUiGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, langList);
                AppUtils.savePreferenceValueForRpc(this, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        public void CreateUIResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("UI.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.UI, checkBoxAvailable.Checked, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseGetVehicleType()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_vehicle_type, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox vehicleTypeChk = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_chk);
            CheckBox make_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_make_cb);
            EditText make_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_make_et);

            CheckBox model_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_model_cb);
            EditText model_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_model_et);

            CheckBox model_year_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_model_year_cb);
            EditText model_year_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_model_year_et);

            CheckBox trim_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_trim_cb);
            EditText trim_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_trim_et);

            CheckBox result_code_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_result_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.vehicle_type_result_code_spinner);

            rpcAlertDialog.SetTitle("VI.GetVehicleType");
            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            make_checkbox.CheckedChange += (sender, e) => make_edittext.Enabled = e.IsChecked;
            model_checkbox.CheckedChange += (sender, e) => model_edittext.Enabled = e.IsChecked;
            model_year_checkbox.CheckedChange += (sender, e) => model_year_edittext.Enabled = e.IsChecked;
            trim_checkbox.CheckedChange += (sender, e) => trim_edittext.Enabled = e.IsChecked;
            vehicleTypeChk.CheckedChange += (sender, e) =>
            {
                make_checkbox.Enabled = e.IsChecked;
                model_checkbox.Enabled = e.IsChecked;
                model_year_checkbox.Enabled = e.IsChecked;
                trim_checkbox.Enabled = e.IsChecked;
                make_edittext.Enabled = e.IsChecked;
                model_edittext.Enabled = e.IsChecked;
                model_year_edittext.Enabled = e.IsChecked;
                trim_edittext.Enabled = e.IsChecked;
            };
            result_code_chk.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                VehicleType vehType = tmpObj.getVehicleType();
                if (vehType != null)
                {
                    make_edittext.Text = vehType.getMake();
                    model_edittext.Text = vehType.getModel();
                    model_year_edittext.Text = vehType.getModelYear();
                    trim_edittext.Text = vehType.getTrim();
                }
            }

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                VehicleType vehType = null;
                if (vehicleTypeChk.Checked)
                {
                    vehType = new VehicleType();
                    if (make_checkbox.Checked) vehType.make = make_edittext.Text;
                    if (model_checkbox.Checked) vehType.model = model_edittext.Text;
                    if (model_year_checkbox.Checked) vehType.modelYear = model_year_edittext.Text;
                    if (trim_checkbox.Checked) vehType.trim = trim_edittext.Text;
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_chk.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetVehicleTypeResponse(BuildRpc.getNextId(), rsltCode, vehType);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = (View)inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("VI.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
            checkBoxAvailable.Checked = true;

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
            }

            checkBoxAvailable.Text = "Available";

            rsltCodeCheckBox.CheckedChange += (sender, e) => resultCodeSpn.Enabled = e.IsChecked;

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                RpcResponse rpcMessage = null;
                rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.VehicleInfo, checkBoxAvailable.Checked, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);

            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.on_touch_event_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetCapabilities");

            CheckBox resultCodeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            resultCodeCheck.Text = "Result Code";

            var resultCodeAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            string[] vrCapabilities = Enum.GetNames(typeof(VrCapabilities));
            bool[] vrCapabilitiesBoolArray = new bool[vrCapabilities.Length];

            ListView vrCapabilitiesListView = rpcView.FindViewById<ListView>(Resource.Id.touch_event_listview);
            List<VrCapabilities> vrCapabilitiesList = new List<VrCapabilities>();
            var vrCapabilitiesAdapter = new VrCapabilitiesAdapter(this, vrCapabilitiesList);
            vrCapabilitiesListView.Adapter = vrCapabilitiesAdapter;

            CheckBox vrCapabilityButtonChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

            resultCodeCheck.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
            vrCapabilityButtonChk.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;

            createTouchEvent.Text = "Add VR Capabilities";
            createTouchEvent.Click += (sender, e1) =>
            {
                Android.Support.V7.App.AlertDialog.Builder vrCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                vrCapabilitiesAlertDialog.SetTitle("VR Capabilities");

                vrCapabilitiesAlertDialog.SetMultiChoiceItems(vrCapabilities, vrCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => vrCapabilitiesBoolArray[e.Which] = e.IsChecked);

                vrCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    vrCapabilitiesAlertDialog.Dispose();
                });

                vrCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    for (int i = 0; i < vrCapabilities.Length; i++)
                    {
                        if (vrCapabilitiesBoolArray[i])
                        {
                            vrCapabilitiesList.Add(((VrCapabilities)typeof(VrCapabilities).GetEnumValues().GetValue(i)));
                        }
                    }
                    vrCapabilitiesAdapter.NotifyDataSetChanged();
                });

                vrCapabilitiesAlertDialog.Show();
            };

            HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getVrCapabilities() != null)
                {
                    vrCapabilitiesList.AddRange(tmpObj.getVrCapabilities());
                    vrCapabilitiesAdapter.NotifyDataSetChanged();
                }
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (resultCodeCheck.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                List<VrCapabilities> list = null;
                if (vrCapabilityButtonChk.Checked)
                {
                    list = vrCapabilitiesList;
                }
                RpcMessage rpcMessage = BuildRpc.buildVrGetCapabilitiesResponse(BuildRpc.getNextId(), selectedResultCode, list);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnLanguage.SetSelection((int)tmpObj.getLanguage());
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? selectedLanguage = null;
                if (languageCheckBox.Checked)
                {
                    selectedLanguage = (Language)spnLanguage.SelectedItemPosition;
                }
                HmiApiLib.Common.Enums.Result? rstldCode = null;

                if (rsltCodeCheckBox.Checked)
                {
                    rstldCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                RpcMessage rpcMessage = BuildRpc.buildVrGetLanguageResponse(BuildRpc.getNextId(), selectedLanguage, rstldCode);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.on_touch_event_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetSupportedLanguages");

            CheckBox resultCodeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            resultCodeCheck.Text = "Result Code";

            resultCodeCheck.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            var resultCodeAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            ListView listView = rpcView.FindViewById<ListView>(Resource.Id.touch_event_listview);
            List<Language> languageList = new List<Language>();
            var adapter = new LanguagesAdapter(this, languageList);
            listView.Adapter = adapter;

            HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getLanguages() != null)
                {
                    languageList.AddRange(tmpObj.getLanguages());
                    adapter.NotifyDataSetChanged();
                }
            }

            bool[] langBoolArray = new bool[languages.Length];

            CheckBox buttonCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

            buttonCheck.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;
            createTouchEvent.Text = "Add Languages";
            createTouchEvent.Click += (sender, e1) =>
            {
                Android.Support.V7.App.AlertDialog.Builder languageAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                languageAlertDialog.SetTitle("Languages");

                languageAlertDialog.SetMultiChoiceItems(languages, langBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => langBoolArray[e.Which] = e.IsChecked);

                languageAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    languageAlertDialog.Dispose();
                });

                languageAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    for (int i = 0; i < languages.Length; i++)
                    {
                        if (langBoolArray[i])
                        {
                            languageList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    adapter.NotifyDataSetChanged();
                });
                languageAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheck.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                List<Language> language = null;
                if (buttonCheck.Checked)
                    language = languageList;

                RpcResponse rpcResponse = BuildRpc.buildVrGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, language);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateBCResponseGetSystemInfo()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_system_info, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("BC.GetSystemInfo");

            CheckBox checkBoxCCPUVesrion = (CheckBox)rpcView.FindViewById(Resource.Id.ccpu_version_cb);
            EditText editTextCCPUVesrion = (EditText)rpcView.FindViewById(Resource.Id.ccpu_version);

            CheckBox checkBoxLanguage = (CheckBox)rpcView.FindViewById(Resource.Id.language_cb);
            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.language);

            CheckBox checkBoxCountryCode = (CheckBox)rpcView.FindViewById(Resource.Id.wers_country_code_cb);
            EditText editTextCountryCode = (EditText)rpcView.FindViewById(Resource.Id.wersCountryCode);

            CheckBox checkBoxResultCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_getSystemInfo_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_code_getSystemInfo);

            checkBoxCCPUVesrion.CheckedChange += (s, e) => editTextCCPUVesrion.Enabled = e.IsChecked;
            checkBoxLanguage.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            checkBoxCountryCode.CheckedChange += (s, e) => editTextCountryCode.Enabled = e.IsChecked;
            checkBoxResultCode.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            spnLanguage.Adapter = languageAdapter;

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                editTextCCPUVesrion.Text = tmpObj.getCcpuVersion();
                spnLanguage.SetSelection((int)tmpObj.getLanguage());
                editTextCountryCode.Text = tmpObj.getWersCountryCode();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("CANCEL", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("TX_LATER", (senderAlert, args) =>
            {
                String countryCode = null;
                String ccpuVersion = null;

                if (checkBoxCCPUVesrion.Checked)
                {
                    ccpuVersion = editTextCCPUVesrion.Text;
                }

                if (checkBoxCountryCode.Checked)
                {
                    countryCode = editTextCountryCode.Text;
                }

                HmiApiLib.Common.Enums.Result? resltCode = null;
                if (checkBoxResultCode.Checked)
                {
                    resltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                Language? language = null;
                if (checkBoxLanguage.Checked)
                {
                    language = (Language)spnLanguage.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildBasicCommunicationGetSystemInfoResponse(BuildRpc.getNextId(), resltCode, ccpuVersion, language, countryCode);
                AppUtils.savePreferenceValueForRpc(spnLanguage.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("RESET", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(spnLanguage.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("VR.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (null != tmpObj.getAvailable())
                {
                    checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                }
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.VR, checkBoxAvailable.Checked, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater layoutInflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = layoutInflater.Inflate(Resource.Layout.ui_get_capabilities, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("UI.GetCapabilities");

            HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());

            Button addDisplayCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilities_add_display_capabilities_btn);
            CheckBox displayCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.ui_get_capability_display_capability_chk);

            Button addAudioPassThruCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilitiess_add_audio_pass_thru_capabilities_btn);
            CheckBox audioPassThruCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.ui_get_capability_audio_pass_thru_capability_chk);

            CheckBox hmiZoneCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_hmi_zone_capabilities_cb);
            Spinner hmiZoneCapabilitiesSpinner = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_hmi_zone_capabilities_spn);

            CheckBox addSoftButtonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_soft_button_capabilities_chk);
            Button addSoftButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilities_add_soft_button_capabilities_btn);
            ListView softButtonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.get_capabilities_soft_button_capabilities_listview);
            List<SoftButtonCapabilities> btnSoftButtonCapList = new List<SoftButtonCapabilities>();
            if (tmpObj != null && tmpObj.getSoftButtonCapabilities() != null)
            {
                btnSoftButtonCapList.AddRange(tmpObj.getSoftButtonCapabilities());
            }
            var adapter = new SoftButtonCapabilitiesAdapter(this, btnSoftButtonCapList);
            softButtonCapabilitiesListView.Adapter = adapter;
            Utility.setListViewHeightBasedOnChildren(softButtonCapabilitiesListView);

            CheckBox HmiCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_hmi_capabilities_chk);
            CheckBox HmiCapabilitiesNavigationCheck = (CheckBox)rpcView.FindViewById(Resource.Id.hmi_capabilities_navigation_checkbox);
            CheckBox HmiCapabilitiesPhonecallCheck = (CheckBox)rpcView.FindViewById(Resource.Id.hmi_capabilities_phone_call_checkbox);

            HmiCapabilitiesCheck.CheckedChange += (sender, e) =>
            {
                HmiCapabilitiesNavigationCheck.Enabled = e.IsChecked;
                HmiCapabilitiesPhonecallCheck.Enabled = e.IsChecked;
            };

            if (tmpObj != null && tmpObj.getHMICapabilities() != null)
            {
                HmiCapabilitiesCheck.Checked = true;
                HmiCapabilitiesNavigationCheck.Checked = tmpObj.getHMICapabilities().getNavigation();
                HmiCapabilitiesPhonecallCheck.Checked = tmpObj.getHMICapabilities().getPhoneCall();
            }

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_cb);
            Spinner rsltCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_spn);

            string[] hmiZoneCapabilities = Enum.GetNames(typeof(HmiZoneCapabilities));
            var hmiZoneCapabilitiesadapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, hmiZoneCapabilities);
            hmiZoneCapabilitiesSpinner.Adapter = hmiZoneCapabilitiesadapter;

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            rsltCodeSpinner.Adapter = resultCodeAdapter;

            if (tmpObj != null)
            {
                rsltCodeSpinner.SetSelection((int)tmpObj.getResultCode());
                hmiZoneCapabilitiesSpinner.SetSelection((int)tmpObj.getHmiZoneCapabilities());
            }

            displayCapabilitiesCheck.CheckedChange += (s, e) => addDisplayCapabilitiesButton.Enabled = e.IsChecked;
            audioPassThruCapabilitiesCheck.CheckedChange += (s, e) => addAudioPassThruCapabilitiesButton.Enabled = e.IsChecked;
            hmiZoneCapabilitiesCheckBox.CheckedChange += (s, e) => hmiZoneCapabilitiesSpinner.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => rsltCodeSpinner.Enabled = e.IsChecked;
            addSoftButtonCapabilitiesCheckBox.CheckedChange += (sender, e) =>
            {
                addSoftButtonCapabilitiesButton.Enabled = e.IsChecked;
                softButtonCapabilitiesListView.Enabled = e.IsChecked;
            };

            AudioPassThruCapabilities audioPassThruCap = null;
            DisplayCapabilities dspCap = null;
            if (tmpObj != null)
            {
                dspCap = tmpObj.getDisplayCapabilities();
                audioPassThruCap = tmpObj.getAudioPassThruCapabilities();
            }

            if (audioPassThruCap == null)
            {
                audioPassThruCap = new AudioPassThruCapabilities();
            }
            if (dspCap == null)
            {
                dspCap = new DisplayCapabilities();
            }

            addDisplayCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder displayCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View displayCapabilitiesView = layoutInflater.Inflate(Resource.Layout.display_capabilities, null);
                displayCapabilitiesAlertDialog.SetView(displayCapabilitiesView);
                displayCapabilitiesAlertDialog.SetTitle("Display Capabilities");

                CheckBox checkBoxDisplayType = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_type_checkbox);
                Spinner spnButtonDisplayType = (Spinner)displayCapabilitiesView.FindViewById(Resource.Id.display_type_spinner);

                string[] displayType = Enum.GetNames(typeof(DisplayType));
                var displayTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, displayType);
                spnButtonDisplayType.Adapter = displayTypeAdapter;
                spnButtonDisplayType.SetSelection((int)dspCap.getDisplayType());

                List<TextField> textFieldsList = new List<TextField>();
                ListView textFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_text_fields_listview);
                Button textFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_btn);
                CheckBox addTextFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_chk);
                if (dspCap.getTextFields() != null)
                {
                    textFieldsList.AddRange(dspCap.getTextFields());
                }
                var textFieldAdapter = new TextFieldAdapter(this, textFieldsList);
                textFieldsListView.Adapter = textFieldAdapter;
                Utility.setListViewHeightBasedOnChildren(textFieldsListView);

                List<ImageField> imageFieldsList = new List<ImageField>();
                ListView imageFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_fields_listview);
                Button imageFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_btn);
                CheckBox imageFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_chk);
                if (dspCap.getImageFields() != null)
                {
                    imageFieldsList.AddRange(dspCap.getImageFields());
                }
                var imageFieldAdapter = new ImageFieldAdapter(this, imageFieldsList);
                imageFieldsListView.Adapter = imageFieldAdapter;
                Utility.setListViewHeightBasedOnChildren(imageFieldsListView);

                List<MediaClockFormat> mediaClockFormatsList = new List<MediaClockFormat>();
                ListView mediaClockFormatsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_media_clock_formats_listview);
                Button mediaClockFormatsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_btn);
                CheckBox mediaClockFormatsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_chk);
                if (dspCap.getMediaClockFormats() != null)
                {
                    mediaClockFormatsList.AddRange(dspCap.getMediaClockFormats());
                }
                var mediaClockFormatAdapter = new MediaClockFormatAdapter(this, mediaClockFormatsList);
                mediaClockFormatsListView.Adapter = mediaClockFormatAdapter;
                Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);

                List<ImageType> imageTypeList = new List<ImageType>();
                ListView imageTypeListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_types_listview);
                CheckBox imageTypeCheckbox = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_chk);
                Button addImageTypeButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_btn);
                if (dspCap.getImageCapabilities() != null)
                {
                    imageTypeList.AddRange(dspCap.getImageCapabilities());
                }
                var imageTypeAdapter = new ImageTypeAdapter(this, imageTypeList);
                imageTypeListView.Adapter = imageTypeAdapter;
                Utility.setListViewHeightBasedOnChildren(imageTypeListView);

                CheckBox screenParamsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_chk);
                Button screenParamsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_btn);

                CheckBox checkBoxGraphicSupported = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_graphic_supported_checkbox);
                checkBoxGraphicSupported.Checked = dspCap.getGraphicSupported();

                CheckBox checkBoxTemplatesAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_checkbox);
                EditText editTextTemplatesAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_edittext);
                if (dspCap.getTemplatesAvailable() != null)
                {
                    String availableTemplates = null;
                    for (int i = 0; i < dspCap.getTemplatesAvailable().Count; i++)
                    {
                        if (i == 0)
                        {
                            availableTemplates = dspCap.getTemplatesAvailable()[i];
                        }
                        else
                        {
                            availableTemplates = availableTemplates + "," + dspCap.getTemplatesAvailable()[i];
                        }
                    }
                    editTextTemplatesAvailable.Text = availableTemplates;
                }

                checkBoxTemplatesAvailable.CheckedChange += (s, e) => editTextTemplatesAvailable.Enabled = e.IsChecked;
                screenParamsChk.CheckedChange += (s, e) => screenParamsButton.Enabled = e.IsChecked;
                checkBoxDisplayType.CheckedChange += (s, e) => spnButtonDisplayType.Enabled = e.IsChecked;
                addTextFieldsChk.CheckedChange += (s, e) =>
                {
                    textFieldsButton.Enabled = e.IsChecked;
                    textFieldsListView.Enabled = e.IsChecked;
                };
                imageFieldsChk.CheckedChange += (s, e) =>
                {
                    imageFieldsButton.Enabled = e.IsChecked;
                    imageFieldsListView.Enabled = e.IsChecked;
                };
                mediaClockFormatsChk.CheckedChange += (sender, e) =>
                {
                    mediaClockFormatsButton.Enabled = e.IsChecked;
                    mediaClockFormatsListView.Enabled = e.IsChecked;
                };

                textFieldsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder textFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    View textFieldsView = layoutInflater.Inflate(Resource.Layout.text_field, null);
                    textFieldsAlertDialog.SetView(textFieldsView);
                    textFieldsAlertDialog.SetTitle("TextField");

                    CheckBox checkBoxName = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_name_checkbox);
                    Spinner spnName = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_name_spinner);

                    string[] textFieldNames = Enum.GetNames(typeof(TextFieldName));
                    var textFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, textFieldNames);
                    spnName.Adapter = textFieldNamesAdapter;

                    CheckBox checkBoxCharacterSet = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_checkbox);
                    Spinner spnCharacterSet = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_spinner);

                    string[] characterSet = Enum.GetNames(typeof(CharacterSet));
                    var characterSetAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, characterSet);
                    spnCharacterSet.Adapter = characterSetAdapter;

                    CheckBox checkBoxWidth = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_width_checkbox);
                    EditText editTextWidth = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_width_edittext);

                    CheckBox checkBoxRow = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_row_checkbox);
                    EditText editTextRow = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_row_edittext);

                    checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                    checkBoxCharacterSet.CheckedChange += (s, e) => spnCharacterSet.Enabled = e.IsChecked;
                    checkBoxWidth.CheckedChange += (s, e) => editTextWidth.Enabled = e.IsChecked;
                    checkBoxRow.CheckedChange += (s, e) => editTextRow.Enabled = e.IsChecked;

                    textFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {
                        TextField txtField = new TextField();

                        if (checkBoxName.Checked)
                        {
                            txtField.name = (TextFieldName)spnName.SelectedItemPosition;
                        }
                        if (checkBoxCharacterSet.Checked)
                        {
                            txtField.characterSet = (CharacterSet)spnCharacterSet.SelectedItemPosition;
                        }
                        if (checkBoxWidth.Checked)
                        {
                            if (editTextWidth.Text != null && editTextWidth.Text.Length > 0)
                                txtField.width = Java.Lang.Integer.ParseInt(editTextWidth.Text);
                        }
                        if (checkBoxRow.Checked)
                        {
                            if (editTextRow.Text != null && editTextRow.Text.Length > 0)
                                txtField.rows = Java.Lang.Integer.ParseInt(editTextRow.Text);
                        }
                        textFieldsList.Add(txtField);
                        textFieldAdapter.NotifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(textFieldsListView);
                    });

                    textFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        textFieldsAlertDialog.Dispose();
                    });
                    textFieldsAlertDialog.Show();
                };

                imageFieldsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder imageFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    View imageFieldsView = layoutInflater.Inflate(Resource.Layout.image_field, null);
                    imageFieldsAlertDialog.SetView(imageFieldsView);
                    imageFieldsAlertDialog.SetTitle("Image Field");

                    CheckBox checkBoxName = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_name_checkbox);
                    Spinner spnName = (Spinner)imageFieldsView.FindViewById(Resource.Id.image_field_name_spinner);

                    string[] imageFieldNames = Enum.GetNames(typeof(ImageFieldName));
                    var imageFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageFieldNames);
                    spnName.Adapter = imageFieldNamesAdapter;

                    List<FileType> fileTypeList = new List<FileType>();
                    ListView fileTypeListView = (ListView)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_listview);
                    var fileTypeAdapter = new FileTypeAdapter(this, fileTypeList);
                    fileTypeListView.Adapter = fileTypeAdapter;

                    string[] fileTypes = Enum.GetNames(typeof(FileType));
                    bool[] fileTypeBoolArray = new bool[fileTypes.Length];

                    CheckBox fileTypeChk = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_chk);
                    Button fileTypeButton = (Button)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_btn);

                    checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                    fileTypeChk.CheckedChange += (sender, e) => fileTypeButton.Enabled = e.IsChecked;

                    fileTypeButton.Click += (sender, e1) =>
                    {
                        Android.Support.V7.App.AlertDialog.Builder fileTypeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                        fileTypeAlertDialog.SetTitle("FileType");

                        fileTypeAlertDialog.SetMultiChoiceItems(fileTypes, fileTypeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => fileTypeBoolArray[e.Which] = e.IsChecked);

                        fileTypeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                        {
                            fileTypeAlertDialog.Dispose();
                        });

                        fileTypeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                        {
                            for (int i = 0; i < fileTypes.Length; i++)
                            {
                                if (fileTypeBoolArray[i])
                                {
                                    fileTypeList.Add(((FileType)typeof(FileType).GetEnumValues().GetValue(i)));
                                }
                            }
                            fileTypeAdapter.NotifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(fileTypeListView);
                        });
                        fileTypeAlertDialog.Show();
                    };

                    CheckBox checkBoxImageResolution = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_resolution_checkbox);

                    CheckBox checkBoxResolutionWidth = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_checkbox);
                    EditText editTextResolutionWidth = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_edit_text);

                    CheckBox checkBoxResolutionHeight = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_checkbox);
                    EditText editTextResolutionHeight = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_edit_text);

                    checkBoxImageResolution.CheckedChange += (sender, e) =>
                    {
                        checkBoxResolutionWidth.Enabled = e.IsChecked;
                        editTextResolutionWidth.Enabled = e.IsChecked;
                        checkBoxResolutionHeight.Enabled = e.IsChecked;
                        editTextResolutionHeight.Enabled = e.IsChecked;
                    };
                    checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                    checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                    imageFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {
                        ImageField imgField = new ImageField();

                        if (checkBoxName.Checked)
                        {
                            imgField.name = (ImageFieldName)spnName.SelectedItemPosition;
                        }
                        if (fileTypeChk.Checked)
                        {
                            imgField.imageTypeSupported = fileTypeList;
                        }

                        ImageResolution resolution = null;
                        if (checkBoxImageResolution.Checked)
                        {
                            resolution = new ImageResolution();

                            if (checkBoxResolutionWidth.Checked)
                            {
                                if (editTextResolutionWidth.Text.Equals(""))
                                    resolution.resolutionWidth = 0;
                                else
                                    resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                            }
                            if (checkBoxResolutionHeight.Checked)
                            {
                                if (editTextResolutionHeight.Text.Equals(""))
                                    resolution.resolutionHeight = 0;
                                else
                                    resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                            }
                        }
                        imgField.imageResolution = resolution;

                        imageFieldsList.Add(imgField);
                        imageFieldAdapter.NotifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(imageFieldsListView);
                    });

                    imageFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        imageFieldsAlertDialog.Dispose();
                    });
                    imageFieldsAlertDialog.Show();
                };

                string[] mediaClockFormats = Enum.GetNames(typeof(MediaClockFormat));
                bool[] mediaClockFormatsBoolArray = new bool[mediaClockFormats.Length];

                mediaClockFormatsButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder mediaClockFormatsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                    mediaClockFormatsAlertDialog.SetTitle("MediaClockFormats");

                    mediaClockFormatsAlertDialog.SetMultiChoiceItems(mediaClockFormats, mediaClockFormatsBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => mediaClockFormatsBoolArray[e.Which] = e.IsChecked);

                    mediaClockFormatsAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        mediaClockFormatsAlertDialog.Dispose();
                    });

                    mediaClockFormatsAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
                        for (int i = 0; i < mediaClockFormats.Length; i++)
                        {
                            if (mediaClockFormatsBoolArray[i])
                            {
                                mediaClockFormatsList.Add(((MediaClockFormat)typeof(MediaClockFormat).GetEnumValues().GetValue(i)));
                            }
                        }
                        mediaClockFormatAdapter.NotifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);
                    });

                    mediaClockFormatsAlertDialog.Show();
                };

                string[] imageCapabilities = Enum.GetNames(typeof(ImageType));
                bool[] imageCapabilitiesBoolArray = new bool[imageCapabilities.Length];

                addImageTypeButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder imageCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                    imageCapabilitiesAlertDialog.SetTitle("Image Capabilities");

                    imageCapabilitiesAlertDialog.SetMultiChoiceItems(imageCapabilities, imageCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => imageCapabilitiesBoolArray[e.Which] = e.IsChecked);

                    imageCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        imageCapabilitiesAlertDialog.Dispose();
                    });

                    imageCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
                        for (int i = 0; i < imageCapabilities.Length; i++)
                        {
                            if (imageCapabilitiesBoolArray[i])
                            {
                                imageTypeList.Add(((ImageType)typeof(ImageType).GetEnumValues().GetValue(i)));
                            }
                        }
                        imageTypeAdapter.NotifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(imageTypeListView);
                    });

                    imageCapabilitiesAlertDialog.Show();
                };

                ScreenParams scrnParam = dspCap.getScreenParams();
                if (null == scrnParam)
                {
                    scrnParam = new ScreenParams();
                }
                screenParamsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder screenParamsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    View screenParamsView = layoutInflater.Inflate(Resource.Layout.screen_param, null);
                    screenParamsAlertDialog.SetView(screenParamsView);
                    screenParamsAlertDialog.SetTitle("Screen Params");

                    CheckBox checkBoxImageResolution = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_image_resolution_checkbox);
                    CheckBox checkBoxResolutionWidth = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_checkbox);
                    EditText editTextResolutionWidth = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_edit_text);

                    CheckBox checkBoxResolutionHeight = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_checkbox);
                    EditText editTextResolutionHeight = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_edit_text);

                    CheckBox checkTouchEventCapabilities = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_touch_event_capability_checkbox);
                    CheckBox checkBoxpressAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_press_available_checkbox);
                    CheckBox checkBoxMultiTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_multi_touch_available_checkbox);
                    CheckBox checkBoxDoubleTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_double_press_available_checkbox);

                    checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                    checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                    checkBoxImageResolution.CheckedChange += (sender, e) =>
                    {
                        checkBoxResolutionWidth.Enabled = e.IsChecked;
                        editTextResolutionWidth.Enabled = e.IsChecked;
                        checkBoxResolutionHeight.Enabled = e.IsChecked;
                        editTextResolutionHeight.Enabled = e.IsChecked;
                    };

                    checkTouchEventCapabilities.CheckedChange += (sender, e) =>
                    {
                        checkBoxpressAvailable.Enabled = e.IsChecked;
                        checkBoxMultiTouchAvailable.Enabled = e.IsChecked;
                        checkBoxDoubleTouchAvailable.Enabled = e.IsChecked;
                    };

                    if (scrnParam.getResolution() != null)
                    {
                        checkBoxImageResolution.Checked = true;
                        editTextResolutionWidth.Text = scrnParam.getResolution().getResolutionWidth().ToString();
                        editTextResolutionHeight.Text = scrnParam.getResolution().getResolutionHeight().ToString();
                    }
                    if (scrnParam.getTouchEventAvailable() != null)
                    {
                        checkTouchEventCapabilities.Checked = true;
                        checkBoxpressAvailable.Checked = scrnParam.getTouchEventAvailable().getPressAvailable();
                        checkBoxMultiTouchAvailable.Checked = scrnParam.getTouchEventAvailable().getMultiTouchAvailable();
                        checkBoxDoubleTouchAvailable.Checked = scrnParam.getTouchEventAvailable().getDoublePressAvailable();
                    }

                    screenParamsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {
                        ImageResolution imgResolution = null;
                        if (checkBoxImageResolution.Checked)
                        {
                            imgResolution = new ImageResolution();
                            if (checkBoxResolutionWidth.Checked)
                            {
                                if (editTextResolutionWidth.Text != null && editTextResolutionWidth.Text.Length > 0)
                                    imgResolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                            }
                            if (checkBoxResolutionHeight.Checked)
                            {
                                if (editTextResolutionHeight.Text != null && editTextResolutionHeight.Text.Length > 0)
                                    imgResolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                            }
                        }
                        scrnParam.resolution = imgResolution;

                        TouchEventCapabilities touchEventCapabilities = null;
                        if (checkTouchEventCapabilities.Checked)
                        {
                            touchEventCapabilities = new TouchEventCapabilities();

                            touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                            touchEventCapabilities.multiTouchAvailable = checkBoxMultiTouchAvailable.Checked;
                            touchEventCapabilities.doublePressAvailable = checkBoxDoubleTouchAvailable.Checked;
                        }
                        scrnParam.touchEventAvailable = touchEventCapabilities;
                    });

                    screenParamsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        screenParamsAlertDialog.Dispose();
                    });
                    screenParamsAlertDialog.Show();
                };

                CheckBox checkBoxNumCustomPresetsAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_checkbox);
                EditText editTextNumCustomPresetsAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_edittext);

                checkBoxNumCustomPresetsAvailable.CheckedChange += (s, e) => editTextNumCustomPresetsAvailable.Enabled = e.IsChecked;

                editTextNumCustomPresetsAvailable.Text = dspCap.getNumCustomPresetsAvailable().ToString();

                displayCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    if (checkBoxDisplayType.Checked)
                    {
                        dspCap.displayType = (DisplayType)spnButtonDisplayType.SelectedItemPosition;
                    }
                    else
                    {
                        dspCap.displayType = DisplayType.CID;
                    }

                    if (addTextFieldsChk.Checked)
                    {
                        dspCap.textFields = textFieldsList;
                    }
                    else
                    {
                        dspCap.textFields = null;
                    }

                    if (imageFieldsChk.Checked)
                    {
                        dspCap.imageFields = imageFieldsList;
                    }
                    else
                    {
                        dspCap.imageFields = null;
                    }

                    if (mediaClockFormatsChk.Checked)
                    {
                        dspCap.mediaClockFormats = mediaClockFormatsList;
                    }
                    else
                    {
                        dspCap.mediaClockFormats = null;
                    }

                    if (imageTypeCheckbox.Checked)
                    {
                        dspCap.imageCapabilities = imageTypeList;
                    }
                    else
                    {
                        dspCap.imageCapabilities = null;
                    }

                    dspCap.graphicSupported = checkBoxGraphicSupported.Checked;
                    if (checkBoxTemplatesAvailable.Checked)
                    {
                        List<String> templatesAvailable = new List<string>();
                        templatesAvailable.AddRange(editTextTemplatesAvailable.Text.Split(','));
                        dspCap.templatesAvailable = templatesAvailable;
                    }
                    else
                    {
                        dspCap.templatesAvailable = null;
                    }

                    if (screenParamsChk.Checked)
                    {
                        dspCap.screenParams = scrnParam;
                    }
                    else
                    {
                        dspCap.screenParams = null;
                    }

                    if (checkBoxNumCustomPresetsAvailable.Checked)
                    {
                        if (editTextNumCustomPresetsAvailable.Text != null && editTextNumCustomPresetsAvailable.Text.Length > 0)
                            dspCap.numCustomPresetsAvailable = Java.Lang.Integer.ParseInt(editTextNumCustomPresetsAvailable.Text);
                    }
                    else
                    {
                        dspCap.numCustomPresetsAvailable = 0;
                    }
                });

                displayCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    displayCapabilitiesAlertDialog.Dispose();
                });
                displayCapabilitiesAlertDialog.Show();
            };

            addAudioPassThruCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder audioPassThruCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View audioPassThruCapabilitiesView = layoutInflater.Inflate(Resource.Layout.audio_pass_thru_capabilities, null);
                audioPassThruCapabilitiesAlertDialog.SetView(audioPassThruCapabilitiesView);
                audioPassThruCapabilitiesAlertDialog.SetTitle("AudioPassThruCapabilities");

                CheckBox samplingRateCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_cap_samlping_rate_cb);
                Spinner samplingRateSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_cap_samlping_rate_spn);

                string[] samplingRate = Enum.GetNames(typeof(SamplingRate));
                var samplingRateAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, samplingRate);
                samplingRateSpn.Adapter = samplingRateAdapter;

                CheckBox bitsPerSampleCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_bits_per_sample_cb);
                Spinner bitsPerSampleSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_bits_per_sample_spn);

                string[] bitsPerSample = Enum.GetNames(typeof(BitsPerSample));
                var bitsPerSampleAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, bitsPerSample);
                bitsPerSampleSpn.Adapter = bitsPerSampleAdapter;

                CheckBox audioTypeCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_audio_type_cb);
                Spinner audioTypeSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_audio_type_spn);

                string[] audioType = Enum.GetNames(typeof(AudioType));
                var audioTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, audioType);
                audioTypeSpn.Adapter = audioTypeAdapter;

                samplingRateCheckBox.CheckedChange += (s, e) => samplingRateSpn.Enabled = e.IsChecked;
                bitsPerSampleCheckBox.CheckedChange += (s, e) => bitsPerSampleSpn.Enabled = e.IsChecked;
                audioTypeCheckBox.CheckedChange += (s, e) => audioTypeSpn.Enabled = e.IsChecked;

                samplingRateSpn.SetSelection((int)audioPassThruCap.getSamplingRate());
                bitsPerSampleSpn.SetSelection((int)audioPassThruCap.getBitsPerSample());
                audioTypeSpn.SetSelection((int)audioPassThruCap.getAudioType());

                audioPassThruCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    if (samplingRateCheckBox.Checked)
                        audioPassThruCap.samplingRate = (SamplingRate)samplingRateSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.samplingRate = SamplingRate.RATE_8KHZ;

                    if (bitsPerSampleCheckBox.Checked)
                        audioPassThruCap.bitsPerSample = (BitsPerSample)bitsPerSampleSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.bitsPerSample = BitsPerSample.RATE_8_BIT;

                    if (audioTypeCheckBox.Checked)
                        audioPassThruCap.audioType = (AudioType)audioTypeSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.audioType = AudioType.PCM;
                });

                audioPassThruCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    audioPassThruCapabilitiesAlertDialog.Dispose();
                });

                audioPassThruCapabilitiesAlertDialog.Show();
            };

            addSoftButtonCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder softButtonCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View softButtonCapabilitiesView = layoutInflater.Inflate(Resource.Layout.soft_button_capabilities, null);
                softButtonCapabilitiesAlertDialog.SetView(softButtonCapabilitiesView);
                softButtonCapabilitiesAlertDialog.SetTitle("SoftButtonCapabilities");

                CheckBox checkBoxShortPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_short_press_available_checkbox);
                CheckBox checkBoxLongPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_long_press_available_checkbox);
                CheckBox checkBoxUpDownAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_up_down_available_checkbox);
                CheckBox checkBoxImageSupported = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_image_supported_checkbox);

                softButtonCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    SoftButtonCapabilities btn = new SoftButtonCapabilities();
                    btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
                    btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
                    btn.upDownAvailable = checkBoxUpDownAvailable.Checked;
                    btn.imageSupported = checkBoxImageSupported.Checked;

                    btnSoftButtonCapList.Add(btn);
                    adapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(softButtonCapabilitiesListView);
                });

                softButtonCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    softButtonCapabilitiesAlertDialog.Dispose();
                });
                softButtonCapabilitiesAlertDialog.Show();

            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HMICapabilities hmiCap = null;
                if (HmiCapabilitiesCheck.Checked)
                {
                    hmiCap = new HMICapabilities();
                    hmiCap.navigation = HmiCapabilitiesNavigationCheck.Checked;
                    hmiCap.phoneCall = HmiCapabilitiesPhonecallCheck.Checked;
                }

                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)rsltCodeSpinner.SelectedItemPosition;
                }

                DisplayCapabilities displayCap = null;
                if (displayCapabilitiesCheck.Checked)
                {
                    displayCap = dspCap;
                }

                AudioPassThruCapabilities audioCap = null;
                if (audioPassThruCapabilitiesCheck.Checked)
                {
                    audioCap = audioPassThruCap;
                }

                HmiZoneCapabilities? selectedHmiZoneCap = null;
                if (hmiZoneCapabilitiesCheckBox.Checked)
                {
                    selectedHmiZoneCap = (HmiZoneCapabilities)hmiZoneCapabilitiesSpinner.SelectedItemPosition;
                }

                List<SoftButtonCapabilities> softBtnCapList = null;
                if (addSoftButtonCapabilitiesCheckBox.Checked)
                {
                    softBtnCapList = btnSoftButtonCapList;
                }

                RpcResponse rpcMessage = BuildRpc.buildUiGetCapabilitiesResponse(BuildRpc.getNextId(), selectedResultCode, displayCap, audioCap, selectedHmiZoneCap, softBtnCapList, hmiCap);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });
            rpcAlertDialog.Show();
        }

        public void CreateButtonsGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.button_get_capabilities_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("Buttons.GetCapabilities");

            List<ButtonCapabilities> btnCap = new List<ButtonCapabilities>();

            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.get_capabilities_list_view);
            var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, btnCap);
            buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;

            Button addCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.add_capabilities_button);
            CheckBox checkBoxOnScreenPresetsAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.on_screen_presets_available);

            CheckBox addButtonCapabilitiesChk = (CheckBox)rpcView.FindViewById(Resource.Id.add_button_capabilities_chk);
            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_tv);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_spn);

            resultCodeCheckbox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
            addButtonCapabilitiesChk.CheckedChange += (sender, e) =>
            {
                addCapabilitiesButton.Enabled = e.IsChecked;
                buttonCapabilitiesListView.Enabled = e.IsChecked;
            };

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getButtonCapabilities() != null)
                {
                    btnCap.AddRange(tmpObj.getButtonCapabilities());
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                }
                if (null != tmpObj.getPresetBankCapabilities())
                {
                    checkBoxOnScreenPresetsAvailable.Checked = tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable();
                }
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            addCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder btnCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View btnCapabilitiesView = inflater.Inflate(Resource.Layout.button_capabilities, null);
                btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
                btnCapabilitiesAlertDialog.SetTitle("Button Capabilities");

                CheckBox chkButtonName = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);
                Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);

                var namesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
                spnButtonNames.Adapter = namesAdapter;

                CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);
                CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);
                CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);

                chkButtonName.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;

                btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    ButtonCapabilities btn = new ButtonCapabilities();
                    if (chkButtonName.Checked)
                    {
                        btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;
                    }

                    btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
                    btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
                    btn.upDownAvailable = checkBoxUpDownAvailable.Checked;

                    btnCap.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                });

                btnCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    btnCapabilitiesAlertDialog.Dispose();
                });
                btnCapabilitiesAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                List<ButtonCapabilities> capabilities = null;
                HmiApiLib.Common.Enums.Result? resultCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    resultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                if (addButtonCapabilitiesChk.Checked)
                {
                    capabilities = btnCap;
                }

                PresetBankCapabilities presetCap = new PresetBankCapabilities();
                presetCap.onScreenPresetsAvailable = checkBoxOnScreenPresetsAvailable.Checked;

                RpcResponse rpcMessage = BuildRpc.buildButtonsGetCapabilitiesResponse(BuildRpc.getNextId(), capabilities, presetCap, resultCode);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseGetVehicleData()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.vi_get_vehicle_data, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VI.GetVehicleData");

            CheckBox result_code_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_chk);
            Spinner result_code_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_spinner);

            CheckBox gps_data_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_chk);
            Button gps_data_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_btn);

            CheckBox speed_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_chk);
            EditText speed_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_et);

            CheckBox rpm_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_chk);
            EditText rpm_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_et);

            CheckBox fuel_level_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_chk);
            EditText fuel_level_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_et);

            CheckBox fuel_level_state_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_chk);
            Spinner fuel_level_state_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_spinner);

            CheckBox instant_fuel_consumption_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_chk);
            EditText instant_fuel_consumption_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_et);

            CheckBox external_temp_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_chk);
            EditText external_temp_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_et);

            CheckBox vin_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_chk);
            EditText vin_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_et);

            CheckBox prndl_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_chk);
            Spinner prndl_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_spinner);

            CheckBox tire_pressure_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_chk);
            Button tire_pressure_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_btn);

            CheckBox odometer_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_chk);
            EditText odometer_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_et);

            CheckBox belt_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_chk);
            Button belt_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_button);

            CheckBox body_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_chk);
            Button body_info_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_button);

            CheckBox device_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_chk);
            Button device_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_button);

            CheckBox driver_braking_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_chk);
            Spinner driver_braking_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_spinner);

            CheckBox wiper_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_chk);
            Spinner wiper_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_spinner);

            CheckBox head_lamp_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_chk);
            Button head_lamp_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_btn);

            CheckBox engine_torque_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_chk);
            EditText engine_torque_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_et);

            CheckBox acc_padel_position_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_chk);
            EditText acc_padel_position_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_et);

            CheckBox steering_wheel_angle_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_chk);
            EditText steering_wheel_angle_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_et);

            CheckBox ecall_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_chk);
            Button ecall_info_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_btn);

            CheckBox airbag_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_chk);
            Button airbag_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_btn);

            CheckBox emergency_event_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_chk);
            Button emergency_event_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_btn);

            CheckBox cluster_modes_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_chk);
            Button cluster_modes_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_btn);

            CheckBox my_key_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_chk);
            Spinner my_key_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_spinner);

			CheckBox electronic_park_brake_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_chk);
			Spinner electronic_park_brake_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_spinner);

            CheckBox engine_oil_life_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_chk);
            EditText engine_oil_life_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_et);

            CheckBox fuel_range_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_chk);
            Button fuel_range_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_btn);
            ListView fuel_range_lv = (ListView)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_lv);
            List<FuelRange> fuelRangeList = null;

            var ElectronicParkBrakeStatus = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, electronicParkBrakeStatusArray);
            electronic_park_brake_status_spinner.Adapter = ElectronicParkBrakeStatus;

            var ResultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            result_code_spinner.Adapter = ResultCodeAdapter;

            var ComponentVolumeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, ComponentVolumeStatusArray);
            fuel_level_state_spinner.Adapter = ComponentVolumeStatusAdapter;

            var PRNDLAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PRNDLArray);
            prndl_spinner.Adapter = PRNDLAdapter;

            var VehicleDataEventStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataEventStatusArray);
            driver_braking_spinner.Adapter = VehicleDataEventStatusAdapter;

            var WiperStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, WiperStatusArray);
            wiper_status_spinner.Adapter = WiperStatusAdapter;

            var VehicleDataStatus = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataStatusArray);

			my_key_spinner.Adapter = WiperStatusAdapter;


			result_code_chk.CheckedChange += (s, e) => result_code_spinner.Enabled = e.IsChecked;
			gps_data_chk.CheckedChange += (s, e) => gps_data_btn.Enabled = e.IsChecked;
			speed_chk.CheckedChange += (s, e) => speed_et.Enabled = e.IsChecked;
			rpm_chk.CheckedChange += (s, e) => rpm_et.Enabled = e.IsChecked;
			fuel_level_chk.CheckedChange += (s, e) => fuel_level_et.Enabled = e.IsChecked;
			fuel_level_state_chk.CheckedChange += (s, e) => fuel_level_state_spinner.Enabled = e.IsChecked;
			instant_fuel_consumption_chk.CheckedChange += (s, e) => instant_fuel_consumption_et.Enabled = e.IsChecked;
			external_temp_chk.CheckedChange += (s, e) => external_temp_et.Enabled = e.IsChecked;
			vin_chk.CheckedChange += (s, e) => vin_et.Enabled = e.IsChecked;
			prndl_chk.CheckedChange += (s, e) => prndl_spinner.Enabled = e.IsChecked;
			tire_pressure_chk.CheckedChange += (s, e) => tire_pressure_btn.Enabled = e.IsChecked;
			odometer_chk.CheckedChange += (s, e) => odometer_et.Enabled = e.IsChecked;
			belt_status_chk.CheckedChange += (s, e) => belt_status_button.Enabled = e.IsChecked;
			body_info_chk.CheckedChange += (s, e) => body_info_button.Enabled = e.IsChecked;
			device_status_chk.CheckedChange += (s, e) => device_status_button.Enabled = e.IsChecked;
			driver_braking_chk.CheckedChange += (s, e) => driver_braking_spinner.Enabled = e.IsChecked;
			wiper_status_chk.CheckedChange += (s, e) => wiper_status_spinner.Enabled = e.IsChecked;
			head_lamp_status_chk.CheckedChange += (s, e) => head_lamp_status_btn.Enabled = e.IsChecked;
			engine_torque_chk.CheckedChange += (s, e) => engine_torque_et.Enabled = e.IsChecked;
			acc_padel_position_chk.CheckedChange += (s, e) => acc_padel_position_et.Enabled = e.IsChecked;
			steering_wheel_angle_chk.CheckedChange += (s, e) => steering_wheel_angle_et.Enabled = e.IsChecked;
			ecall_info_chk.CheckedChange += (s, e) => ecall_info_btn.Enabled = e.IsChecked;
			airbag_status_chk.CheckedChange += (s, e) => airbag_status_btn.Enabled = e.IsChecked;
			emergency_event_chk.CheckedChange += (s, e) => emergency_event_btn.Enabled = e.IsChecked;
			cluster_modes_chk.CheckedChange += (s, e) => cluster_modes_btn.Enabled = e.IsChecked;
			my_key_chk.CheckedChange += (s, e) => my_key_spinner.Enabled = e.IsChecked;
			electronic_park_brake_status_chk.CheckedChange += (s, e) => electronic_park_brake_status_spinner.Enabled = e.IsChecked;
            engine_oil_life_chk.CheckedChange += (s, e) => engine_oil_life_et.Enabled = e.IsChecked;
            fuel_range_chk.CheckedChange += (s, e) =>
            {
                fuel_range_btn.Enabled = e.IsChecked;
                fuel_range_lv.Enabled = e.IsChecked;
            };

			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();

            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(this, tmpObj.getMethod());

            GPSData viGPSData = null;
            TireStatus viTireStatus = null;
            BeltStatus viBeltStatus = null;
            BodyInformation viBodyInformation = null;
            DeviceStatus viDeviceStatus = null;
            HeadLampStatus viHeadLampStatus = null;
            ECallInfo viECallInfo = null;
            AirbagStatus viAirbagStatus = null;
            EmergencyEvent viEmergencyEvent = null;
            ClusterModeStatus viClusterModeStatus = null;

            if (tmpObj != null)
            {
                result_code_spinner.SetSelection((int)tmpObj.getResultCode());
                viGPSData = tmpObj.getGps();
                if (tmpObj.getSpeed() != null)
                {
                    speed_et.Text = tmpObj.getSpeed().ToString();
                }
                if (tmpObj.getRpm() != null)
                {
                    rpm_et.Text = tmpObj.getRpm().ToString();
                }
                if (tmpObj.getFuelLevel() != null)
                {
                    fuel_level_et.Text = tmpObj.getFuelLevel().ToString();
                }
                if (tmpObj.getFuelLevel_State() != null)
                {
                    fuel_level_state_spinner.SetSelection((int)tmpObj.getFuelLevel_State());
                }
                if (tmpObj.getInstantFuelConsumption() != null)
                {
                    instant_fuel_consumption_et.Text = tmpObj.getInstantFuelConsumption().ToString();
                }
                if (tmpObj.getExternalTemperature() != null)
                {
                    external_temp_et.Text = tmpObj.getExternalTemperature().ToString();
                }
                if (tmpObj.getVin() != null)
                {
                    vin_et.Text = tmpObj.getVin().ToString();
                }

                if (tmpObj.getPrndl() != null)
                {
                    prndl_spinner.SetSelection((int)tmpObj.getPrndl());
                }
                viTireStatus = tmpObj.getTirePressure();
                if (tmpObj.getOdometer() != null)
                {
                    odometer_et.Text = tmpObj.getOdometer().ToString();
                }
                viBeltStatus = tmpObj.getBeltStatus();
                viBodyInformation = tmpObj.getBodyInformation();
                viDeviceStatus = tmpObj.getDeviceStatus();

                if (tmpObj.getDriverBraking() != null)
                {
                    driver_braking_spinner.SetSelection((int)tmpObj.getDriverBraking());
                }

                if (tmpObj.getWiperStatus() != null)
                {
                    wiper_status_spinner.SetSelection((int)tmpObj.getWiperStatus());
                }
                viHeadLampStatus = tmpObj.getHeadLampStatus();
                if (tmpObj.getEngineTorque() != null)
                {
                    engine_torque_et.Text = tmpObj.getEngineTorque().ToString();
                }
                if (tmpObj.getAccPedalPosition() != null)
                {
                    acc_padel_position_et.Text = tmpObj.getAccPedalPosition().ToString();
                }
                if (tmpObj.getSteeringWheelAngle() != null)
                {
                    steering_wheel_angle_et.Text = tmpObj.getSteeringWheelAngle().ToString();
                }
                viECallInfo = tmpObj.getECallInfo();
                viAirbagStatus = tmpObj.getAirbagStatus();
                viEmergencyEvent = tmpObj.getEmergencyEvent();
                viClusterModeStatus = tmpObj.getClusterModes();
                if (tmpObj.getMyKey() != null)
                {
                    my_key_spinner.SetSelection((int)tmpObj.getMyKey().getE911Override());
                }
                if (tmpObj.getElectronicParkBrakeStatus() != null)
                {
                    electronic_park_brake_status_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus());
                }
                if (tmpObj.getEngineOilLife() != null)
                {
                    engine_oil_life_et.Text = ((float)tmpObj.getEngineOilLife()).ToString();
                }
                fuelRangeList = tmpObj.getFuelRange();
			}

            if (fuelRangeList == null)
                fuelRangeList = new List<FuelRange>();

            var fuelRangeAdapter = new FuelRangeAdapter(this, fuelRangeList);
            fuel_range_lv.Adapter = fuelRangeAdapter;
            Utility.setListViewHeightBasedOnChildren(fuel_range_lv);

            gps_data_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder gpsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View gpsView = inflater.Inflate(Resource.Layout.vi_gps_data, null);
                gpsAlertDialog.SetView(gpsView);
                gpsAlertDialog.SetTitle("GPS Data");

                CheckBox chkLongitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_chk);
                EditText etLongitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_et);

                CheckBox chkLatitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_chk);
                EditText etLatitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_et);

                CheckBox chkUtcYear = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_chk);
                EditText etUtcYear = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_et);

                CheckBox chkUtcMonth = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_chk);
                EditText etUtcMonth = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_et);

                CheckBox chkUtcDay = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_chk);
                EditText etUtcDay = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_et);

                CheckBox chkUtcHours = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_chk);
                EditText etUtcHours = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_et);

                CheckBox chkUtcMinutes = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_chk);
                EditText etUtcMinutes = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_et);

                CheckBox chkUtcSeconds = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_chk);
                EditText etUtcSeconds = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_et);

                CheckBox chkCompassDirection = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_chk);
                Spinner spnCompassDirection = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_spinner);

                CheckBox chkpdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_chk);
                EditText etpdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_et);

                CheckBox chkhdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_chk);
                EditText ethdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_et);

                CheckBox chkvdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_chk);
                EditText etvdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_et);

                CheckBox chkActual = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_actual_chk);

                CheckBox chkSatellites = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_chk);
                EditText etSatellites = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_et);

                CheckBox chkDimension = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_chk);
                Spinner spnDimension = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_spinner);

                CheckBox chkAltitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_chk);
                EditText etAltitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_et);

                CheckBox chkHeading = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_chk);
                EditText etHeading = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_et);

                CheckBox chkSpeed = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_chk);
                EditText etSpeed = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_et);

                var CompassDirectionAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, CompassDirectionArray);
                spnCompassDirection.Adapter = CompassDirectionAdapter;

                var DimensionAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, DimensionArray);
                spnDimension.Adapter = DimensionAdapter;

                chkLongitude.CheckedChange += (sender1, e1) => etLongitude.Enabled = e1.IsChecked;
                chkLatitude.CheckedChange += (sender1, e1) => etLatitude.Enabled = e1.IsChecked;
                chkUtcYear.CheckedChange += (sender1, e1) => etUtcYear.Enabled = e1.IsChecked;
                chkUtcMonth.CheckedChange += (sender1, e1) => etUtcMonth.Enabled = e1.IsChecked;
                chkUtcDay.CheckedChange += (sender1, e1) => etUtcDay.Enabled = e1.IsChecked;
                chkUtcHours.CheckedChange += (sender1, e1) => etUtcHours.Enabled = e1.IsChecked;
                chkUtcMinutes.CheckedChange += (sender1, e1) => etUtcMinutes.Enabled = e1.IsChecked;
                chkUtcSeconds.CheckedChange += (sender1, e1) => etUtcSeconds.Enabled = e1.IsChecked;
                chkCompassDirection.CheckedChange += (sender1, e1) => spnCompassDirection.Enabled = e1.IsChecked;
                chkpdop.CheckedChange += (sender1, e1) => etpdop.Enabled = e1.IsChecked;
                chkhdop.CheckedChange += (sender1, e1) => ethdop.Enabled = e1.IsChecked;
                chkvdop.CheckedChange += (sender1, e1) => etvdop.Enabled = e1.IsChecked;
                chkSatellites.CheckedChange += (sender1, e1) => etSatellites.Enabled = e1.IsChecked;
                chkDimension.CheckedChange += (sender1, e1) => spnDimension.Enabled = e1.IsChecked;
                chkAltitude.CheckedChange += (sender1, e1) => etAltitude.Enabled = e1.IsChecked;
                chkHeading.CheckedChange += (sender1, e1) => etHeading.Enabled = e1.IsChecked;
                chkSpeed.CheckedChange += (sender1, e1) => etSpeed.Enabled = e1.IsChecked;

                if (viGPSData != null)
                {
                    etLongitude.Text = viGPSData.getLongitudeDegrees().ToString();
                    etLatitude.Text = viGPSData.getLatitudeDegrees().ToString();
                    etUtcYear.Text = viGPSData.getUtcYear().ToString();
                    etUtcMonth.Text = viGPSData.getUtcMonth().ToString();
                    etUtcDay.Text = viGPSData.getUtcDay().ToString();
                    etUtcHours.Text = viGPSData.getUtcHours().ToString();
                    etUtcMinutes.Text = viGPSData.getUtcMinutes().ToString();
                    etUtcSeconds.Text = viGPSData.getUtcSeconds().ToString();
                    spnCompassDirection.SetSelection((int)viGPSData.getCompassDirection());
                    etpdop.Text = viGPSData.getPdop().ToString();
                    ethdop.Text = viGPSData.getHdop().ToString();
                    etvdop.Text = viGPSData.getVdop().ToString();
                    chkActual.Checked = viGPSData.getActual();
                    etSatellites.Text = viGPSData.getSatellites().ToString();
                    spnDimension.SetSelection((int)viGPSData.getDimension());
                    etAltitude.Text = viGPSData.getAltitude().ToString();
                    etHeading.Text = viGPSData.getHeading().ToString();
                    etSpeed.Text = viGPSData.getSpeed().ToString();
                }
                else
                {
                    viGPSData = new GPSData();
                }
                gpsAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    gpsAlertDialog.Dispose();
                });
                gpsAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (chkLongitude.Checked && etLongitude.Text != null && etLongitude.Text.Length > 0)
                    {
                        viGPSData.longitudeDegrees = Java.Lang.Float.ParseFloat(etLongitude.Text);
                    }
                    if (chkLatitude.Checked && etLatitude.Text != null && etLatitude.Text.Length > 0)
                    {
                        viGPSData.latitudeDegrees = Java.Lang.Float.ParseFloat(etLatitude.Text);
                    }
                    if (chkUtcYear.Checked && etUtcYear.Text != null && etUtcYear.Text.Length > 0)
                    {
                        viGPSData.utcYear = Java.Lang.Integer.ParseInt(etUtcYear.Text);
                    }
                    if (chkUtcMonth.Checked && etUtcMonth.Text != null && etUtcMonth.Text.Length > 0)
                    {
                        viGPSData.utcMonth = Java.Lang.Integer.ParseInt(etUtcMonth.Text);
                    }
                    if (chkUtcDay.Checked && etUtcDay.Text != null && etUtcDay.Text.Length > 0)
                    {
                        viGPSData.utcDay = Java.Lang.Integer.ParseInt(etUtcDay.Text);
                    }
                    if (chkUtcHours.Checked && etUtcHours.Text != null && etUtcHours.Text.Length > 0)
                    {
                        viGPSData.utcHours = Java.Lang.Integer.ParseInt(etUtcHours.Text);
                    }
                    if (chkUtcMinutes.Checked && etUtcMinutes.Text != null && etUtcMinutes.Text.Length > 0)
                    {
                        viGPSData.utcMinutes = Java.Lang.Integer.ParseInt(etUtcMinutes.Text);
                    }
                    if (chkUtcSeconds.Checked && etUtcSeconds.Text != null && etUtcSeconds.Text.Length > 0)
                    {
                        viGPSData.utcSeconds = Java.Lang.Integer.ParseInt(etUtcSeconds.Text);
                    }
                    if (chkCompassDirection.Checked)
                    {
                        viGPSData.compassDirection = (CompassDirection)spnCompassDirection.SelectedItemPosition;
                    }
                    if (chkpdop.Checked && etpdop.Text != null && etpdop.Text.Length > 0)
                    {
                        viGPSData.pdop = Java.Lang.Float.ParseFloat(etpdop.Text);
                    }
                    if (chkhdop.Checked && ethdop.Text != null && ethdop.Text.Length > 0)
                    {
                        viGPSData.hdop = Java.Lang.Float.ParseFloat(ethdop.Text);
                    }
                    if (chkvdop.Checked && etvdop.Text != null && etvdop.Text.Length > 0)
                    {
                        viGPSData.vdop = Java.Lang.Float.ParseFloat(etvdop.Text);
                    }
                    viGPSData.actual = chkActual.Checked;
                    if (chkSatellites.Checked && etSatellites.Text != null && etSatellites.Text.Length > 0)
                    {
                        viGPSData.satellites = Java.Lang.Integer.ParseInt(etSatellites.Text);
                    }
                    if (chkDimension.Checked)
                    {
                        viGPSData.dimension = (Dimension)spnDimension.SelectedItemPosition;
                    }
                    if (chkAltitude.Checked && etAltitude.Text != null && etAltitude.Text.Length > 0)
                    {
                        viGPSData.altitude = Java.Lang.Float.ParseFloat(etAltitude.Text);
                    }
                    if (chkHeading.Checked && etHeading.Text != null && etHeading.Text.Length > 0)
                    {
                        viGPSData.heading = Java.Lang.Float.ParseFloat(etHeading.Text);
                    }
                    if (chkSpeed.Checked && etSpeed.Text != null && etSpeed.Text.Length > 0)
                    {
                        viGPSData.speed = Java.Lang.Float.ParseFloat(etSpeed.Text);
                    }
                });
                gpsAlertDialog.Show();
            };

            tire_pressure_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder tirePressureAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View tirePressureView = inflater.Inflate(Resource.Layout.vi_tire_status, null);
                tirePressureAlertDialog.SetView(tirePressureView);
                tirePressureAlertDialog.SetTitle("Tire Pressure");

                CheckBox pressure_tell_tale_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_chk);
                Spinner pressure_tell_tale_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_spinner);

                CheckBox left_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_chk);
                Spinner left_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_spinner);

                CheckBox right_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_chk);
                Spinner right_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_spinner);

                CheckBox left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_chk);
                Spinner left_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_spinner);

                CheckBox right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_chk);
                Spinner right_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_spinner);

                CheckBox inner_left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_chk);
                Spinner inner_left_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_spinner);

                CheckBox inner_right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_chk);
                Spinner inner_right_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_spinner);

                var WarningLightStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, WarningLightStatusArray);
                pressure_tell_tale_spn.Adapter = WarningLightStatusAdapter;

                var SingleTireStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, SingleTireStatusArray);
                left_front_spn.Adapter = SingleTireStatusAdapter;
                right_front_spn.Adapter = SingleTireStatusAdapter;
                left_rear_spn.Adapter = SingleTireStatusAdapter;
                right_rear_spn.Adapter = SingleTireStatusAdapter;
                inner_left_rear_spinner.Adapter = SingleTireStatusAdapter;
                inner_right_rear_spinner.Adapter = SingleTireStatusAdapter;

                pressure_tell_tale_chk.CheckedChange += (sender1, e1) => pressure_tell_tale_spn.Enabled = e1.IsChecked;
                left_front_chk.CheckedChange += (sender1, e1) => left_front_spn.Enabled = e1.IsChecked;
                right_front_chk.CheckedChange += (sender1, e1) => right_front_spn.Enabled = e1.IsChecked;
                left_rear_chk.CheckedChange += (sender1, e1) => left_rear_spn.Enabled = e1.IsChecked;
                right_rear_chk.CheckedChange += (sender1, e1) => right_rear_spn.Enabled = e1.IsChecked;
                inner_left_rear_chk.CheckedChange += (sender1, e1) => inner_left_rear_spinner.Enabled = e1.IsChecked;
                inner_right_rear_chk.CheckedChange += (sender1, e1) => inner_right_rear_spinner.Enabled = e1.IsChecked;

                if (viTireStatus != null)
                {
                    pressure_tell_tale_spn.SetSelection((int)viTireStatus.getPressureTelltale());
                    if (viTireStatus.getleftFront() != null)
                    {
                        left_front_spn.SetSelection((int)viTireStatus.getleftFront().getStatus());
                    }
                    if (viTireStatus.getRightFront() != null)
                    {
                        right_front_spn.SetSelection((int)viTireStatus.getRightFront().getStatus());
                    }
                    if (viTireStatus.getLeftRear() != null)
                    {
                        left_rear_spn.SetSelection((int)viTireStatus.getLeftRear().getStatus());
                    }
                    if (viTireStatus.getRightRear() != null)
                    {
                        right_rear_spn.SetSelection((int)viTireStatus.getRightRear().getStatus());
                    }
                    if (viTireStatus.getInnerLeftRear() != null)
                    {
                        inner_left_rear_spinner.SetSelection((int)viTireStatus.getInnerLeftRear().getStatus());
                    }
                    if (viTireStatus.getInnerRightRear() != null)
                    {
                        inner_right_rear_spinner.SetSelection((int)viTireStatus.getInnerRightRear().getStatus());
                    }
                }
                else
                {
                    viTireStatus = new TireStatus();
                }

                tirePressureAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    tirePressureAlertDialog.Dispose();
                });
                tirePressureAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (pressure_tell_tale_chk.Checked)
                    {
                        viTireStatus.pressureTelltale = (WarningLightStatus)pressure_tell_tale_spn.SelectedItemPosition;
                    }
                    if (left_front_chk.Checked)
                    {
                        SingleTireStatus leftFront = new SingleTireStatus();
                        leftFront.status = (ComponentVolumeStatus)left_front_spn.SelectedItemPosition;
                        viTireStatus.leftFront = leftFront;
                    }
                    if (right_front_chk.Checked)
                    {
                        SingleTireStatus right_front = new SingleTireStatus();
                        right_front.status = (ComponentVolumeStatus)right_front_spn.SelectedItemPosition;
                        viTireStatus.rightFront = right_front;
                    }
                    if (left_rear_chk.Checked)
                    {
                        SingleTireStatus left_rear = new SingleTireStatus();
                        left_rear.status = (ComponentVolumeStatus)left_rear_spn.SelectedItemPosition;
                        viTireStatus.leftRear = left_rear;
                    }
                    if (right_rear_chk.Checked)
                    {
                        SingleTireStatus right_rear = new SingleTireStatus();
                        right_rear.status = (ComponentVolumeStatus)right_rear_spn.SelectedItemPosition;
                        viTireStatus.rightRear = right_rear;
                    }
                    if (inner_left_rear_chk.Checked)
                    {
                        SingleTireStatus inner_left_rear = new SingleTireStatus();
                        inner_left_rear.status = (ComponentVolumeStatus)inner_left_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerLeftRear = inner_left_rear;
                    }
                    if (inner_right_rear_chk.Checked)
                    {
                        SingleTireStatus inner_right_rear = new SingleTireStatus();
                        inner_right_rear.status = (ComponentVolumeStatus)inner_right_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerRightRear = inner_right_rear;
                    }
                });
                tirePressureAlertDialog.Show();
            };

            belt_status_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder beltStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View beltStatusView = inflater.Inflate(Resource.Layout.vi_belt_status, null);
                beltStatusAlertDialog.SetView(beltStatusView);
                beltStatusAlertDialog.SetTitle("Belt Status");

                CheckBox driver_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_chk);
                Spinner driver_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_spinner);

                CheckBox passenger_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_chk);
                Spinner passenger_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_spinner);

                CheckBox passenger_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_chk);
                Spinner passenger_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_spinner);

                CheckBox driver_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_chk);
                Spinner driver_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_spinner);

                CheckBox left_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_chk);
                Spinner left_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_spinner);

                CheckBox passenger_child_detected_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_chk);
                Spinner passenger_child_detected_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_spinner);

                CheckBox right_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_chk);
                Spinner right_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_spinner);

                CheckBox middle_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_chk);
                Spinner middle_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_spinner);

                CheckBox middle_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_chk);
                Spinner middle_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_spinner);

                CheckBox left_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_chk);
                Spinner left_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_spinner);

                CheckBox right_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_chk);
                Spinner right_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_spinner);

                CheckBox left_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_chk);
                Spinner left_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_spinner);

                CheckBox right_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_chk);
                Spinner right_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_spinner);

                CheckBox middle_row_1_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_chk);
                Spinner middle_row_1_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_spinner);

                CheckBox middle_row_1_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_chk);
                Spinner middle_row_1_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_spinner);

                driver_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_child_detected_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_1_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_1_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;

                if (viBeltStatus != null)
                {
                    driver_belt_deployed_spinner.SetSelection((int)viBeltStatus.getDriverBeltDeployed());
                    passenger_belt_deployed_spinner.SetSelection((int)viBeltStatus.getPassengerBeltDeployed());
                    passenger_buckle_belted_spinner.SetSelection((int)viBeltStatus.getPassengerBuckleBelted());
                    driver_buckle_belted_spinner.SetSelection((int)viBeltStatus.getDriverBuckleBelted());
                    left_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow2BuckleBelted());
                    passenger_child_detected_spinner.SetSelection((int)viBeltStatus.getPassengerChildDetected());
                    right_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow2BuckleBelted());
                    middle_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow2BuckleBelted());
                    middle_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow3BuckleBelted());
                    left_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow3BuckleBelted());
                    right_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow3BuckleBelted());
                    left_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getLeftRearInflatableBelted());
                    right_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getRightRearInflatableBelted());
                    middle_row_1_belt_deployed_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BeltDeployed());
                    middle_row_1_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BuckleBelted());
                }
                else
                {
                    viBeltStatus = new BeltStatus();
                }

                driver_belt_deployed_chk.CheckedChange += (sender1, e1) => driver_belt_deployed_spinner.Enabled = e1.IsChecked;
                passenger_belt_deployed_chk.CheckedChange += (sender1, e1) => passenger_belt_deployed_spinner.Enabled = e1.IsChecked;
                passenger_buckle_belted_chk.CheckedChange += (sender1, e1) => passenger_buckle_belted_spinner.Enabled = e1.IsChecked;
                driver_buckle_belted_chk.CheckedChange += (sender1, e1) => driver_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                passenger_child_detected_chk.CheckedChange += (sender1, e1) => passenger_child_detected_spinner.Enabled = e1.IsChecked;
                right_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                middle_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                middle_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                right_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => left_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
                right_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => right_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
                middle_row_1_belt_deployed_chk.CheckedChange += (sender1, e1) => middle_row_1_belt_deployed_spinner.Enabled = e1.IsChecked;
                middle_row_1_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_1_buckle_belted_spinner.Enabled = e1.IsChecked;

                beltStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    beltStatusAlertDialog.Dispose();
                });
                beltStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (driver_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.driverBeltDeployed = (VehicleDataEventStatus)driver_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.passengerBeltDeployed = (VehicleDataEventStatus)passenger_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.passengerBuckleBelted = (VehicleDataEventStatus)passenger_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (driver_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.driverBuckleBelted = (VehicleDataEventStatus)driver_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.leftRow2BuckleBelted = (VehicleDataEventStatus)left_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (passenger_child_detected_chk.Checked)
                    {
                        viBeltStatus.passengerChildDetected = (VehicleDataEventStatus)passenger_child_detected_spinner.SelectedItemPosition;
                    }
                    if (right_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.rightRow2BuckleBelted = (VehicleDataEventStatus)right_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow2BuckleBelted = (VehicleDataEventStatus)middle_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow3BuckleBelted = (VehicleDataEventStatus)middle_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.leftRow3BuckleBelted = (VehicleDataEventStatus)left_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (right_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.rightRow3BuckleBelted = (VehicleDataEventStatus)right_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_rear_inflatable_belted_chk.Checked)
                    {
                        viBeltStatus.leftRearInflatableBelted = (VehicleDataEventStatus)left_rear_inflatable_belted_spinner.SelectedItemPosition;
                    }
                    if (right_rear_inflatable_belted_chk.Checked)
                    {
                        viBeltStatus.rightRearInflatableBelted = (VehicleDataEventStatus)right_rear_inflatable_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_1_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.middleRow1BeltDeployed = (VehicleDataEventStatus)middle_row_1_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (middle_row_1_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow1BuckleBelted = (VehicleDataEventStatus)middle_row_1_buckle_belted_spinner.SelectedItemPosition;
                    }
                });
                beltStatusAlertDialog.Show();
            };

            body_info_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder bodyInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View bodyInfoView = inflater.Inflate(Resource.Layout.vi_body_information, null);
                bodyInfoAlertDialog.SetView(bodyInfoView);
                bodyInfoAlertDialog.SetTitle("Body Info");

                CheckBox park_brake_active = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active);

                CheckBox ignition_stable_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_chk);
                Spinner ignition_stable_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_spinner);

                CheckBox ignition_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_chk);
                Spinner ignition_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_spinner);

                CheckBox driver_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar);
                CheckBox passenger_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar);
                CheckBox rear_left_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar);
                CheckBox rear_right_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar);

                var IgnitionStableStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStableStatusArray);
                ignition_stable_status_spinner.Adapter = IgnitionStableStatusAdapter;

                var IgnitionStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStatusArray);
                ignition_status_spinner.Adapter = IgnitionStatusAdapter;

                ignition_stable_status_chk.CheckedChange += (sender1, e1) => ignition_stable_status_spinner.Enabled = e1.IsChecked;
                ignition_status_chk.CheckedChange += (sender1, e1) => ignition_status_spinner.Enabled = e1.IsChecked;

                if (viBodyInformation != null)
                {
                    park_brake_active.Checked = viBodyInformation.getParkBrakeActive();
                    ignition_stable_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStableStatus());
                    ignition_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStatus());
                    driver_door_ajar.Checked = viBodyInformation.getDriverDoorAjar();
                    passenger_door_ajar.Checked = viBodyInformation.getPassengerDoorAjar();
                    rear_left_door_ajar.Checked = viBodyInformation.getRearLeftDoorAjar();
                    rear_right_door_ajar.Checked = viBodyInformation.getRearRightDoorAjar();
                }
                else
                {
                    viBodyInformation = new BodyInformation();
                }

                bodyInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    bodyInfoAlertDialog.Dispose();
                });
                bodyInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viBodyInformation.parkBrakeActive = park_brake_active.Checked;
                    if (ignition_stable_status_chk.Checked)
                    {
                        viBodyInformation.ignitionStableStatus = (IgnitionStableStatus)ignition_stable_status_spinner.SelectedItemPosition;
                    }
                    if (ignition_status_chk.Checked)
                    {
                        viBodyInformation.ignitionStatus = (IgnitionStatus)ignition_status_spinner.SelectedItemPosition;
                    }
                    viBodyInformation.driverDoorAjar = driver_door_ajar.Checked;
                    viBodyInformation.passengerDoorAjar = passenger_door_ajar.Checked;
                    viBodyInformation.rearLeftDoorAjar = rear_left_door_ajar.Checked;
                    viBodyInformation.rearRightDoorAjar = rear_right_door_ajar.Checked;
                });
                bodyInfoAlertDialog.Show();
            };

            device_status_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder deviceStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View deviceStatusView = inflater.Inflate(Resource.Layout.vi_device_status, null);
                deviceStatusAlertDialog.SetView(deviceStatusView);
                deviceStatusAlertDialog.SetTitle("Device Status");

                CheckBox voice_rec_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on);
                CheckBox bt_icon_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on);
                CheckBox call_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_call_active);
                CheckBox phone_roaming = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming);
                CheckBox text_msg_available = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available);

                CheckBox batt_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_chk);
                Spinner batt_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_spinner);

                CheckBox stereo_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted);
                CheckBox mono_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted);

                CheckBox signal_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_chk);
                Spinner signal_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_spinner);

                CheckBox primary_audio_source_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_chk);
                Spinner primary_audio_source_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_spinner);

                CheckBox ecall_event_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active);

                var battLevelStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, DeviceLevelStatusArray);
                batt_level_status_spinner.Adapter = battLevelStatusAdapter;
                signal_level_status_spinner.Adapter = battLevelStatusAdapter;

                var PrimaryAudioSourceAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PrimaryAudioSourceArray);
                primary_audio_source_spinner.Adapter = PrimaryAudioSourceAdapter;

                batt_level_status_chk.CheckedChange += (sender1, e1) => batt_level_status_spinner.Enabled = e1.IsChecked;
                signal_level_status_chk.CheckedChange += (sender1, e1) => signal_level_status_spinner.Enabled = e1.IsChecked;
                primary_audio_source_chk.CheckedChange += (sender1, e1) => primary_audio_source_spinner.Enabled = e1.IsChecked;

                if (viDeviceStatus != null)
                {
                    voice_rec_on.Checked = viDeviceStatus.voiceRecOn;
                    bt_icon_on.Checked = viDeviceStatus.btIconOn;
                    call_active.Checked = viDeviceStatus.callActive;
                    phone_roaming.Checked = viDeviceStatus.phoneRoaming;
                    text_msg_available.Checked = viDeviceStatus.textMsgAvailable;
                    batt_level_status_spinner.SetSelection((int)viDeviceStatus.getBattLevelStatus());
                    stereo_audio_output_muted.Checked = viDeviceStatus.stereoAudioOutputMuted;
                    mono_audio_output_muted.Checked = viDeviceStatus.monoAudioOutputMuted;
                    signal_level_status_spinner.SetSelection((int)viDeviceStatus.getSignalLevelStatus());
                    primary_audio_source_spinner.SetSelection((int)viDeviceStatus.getPrimaryAudioSource());
                    ecall_event_active.Checked = viDeviceStatus.eCallEventActive;
                }
                else
                {
                    viDeviceStatus = new DeviceStatus();
                }

                deviceStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    deviceStatusAlertDialog.Dispose();
                });
                deviceStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viDeviceStatus.voiceRecOn = voice_rec_on.Checked;
                    viDeviceStatus.btIconOn = bt_icon_on.Checked;
                    viDeviceStatus.callActive = call_active.Checked;
                    viDeviceStatus.phoneRoaming = phone_roaming.Checked;
                    viDeviceStatus.textMsgAvailable = text_msg_available.Checked;
                    if (batt_level_status_chk.Checked)
                    {
                        viDeviceStatus.battLevelStatus = (DeviceLevelStatus)batt_level_status_spinner.SelectedItemPosition;
                    }
                    viDeviceStatus.stereoAudioOutputMuted = stereo_audio_output_muted.Checked;
                    viDeviceStatus.monoAudioOutputMuted = mono_audio_output_muted.Checked;
                    if (signal_level_status_chk.Checked)
                    {
                        viDeviceStatus.signalLevelStatus = (DeviceLevelStatus)signal_level_status_spinner.SelectedItemPosition;
                    }
                    if (primary_audio_source_chk.Checked)
                    {
                        viDeviceStatus.primaryAudioSource = (PrimaryAudioSource)primary_audio_source_spinner.SelectedItemPosition;
                    }
                    viDeviceStatus.eCallEventActive = ecall_event_active.Checked;
                });
                deviceStatusAlertDialog.Show();
            };

            head_lamp_status_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder headLampAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View headLampView = inflater.Inflate(Resource.Layout.vi_head_lamp_status, null);
                headLampAlertDialog.SetView(headLampView);
                headLampAlertDialog.SetTitle("Head Lamp Status");

                CheckBox low_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on);
                CheckBox high_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on);

                CheckBox ambient_light_sensor_status_chk = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_chk);
                Spinner ambient_light_sensor_status_spinner = (Spinner)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_spinner);

                var AmbientLightStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, AmbientLightStatusArray);
                ambient_light_sensor_status_spinner.Adapter = AmbientLightStatusAdapter;

                ambient_light_sensor_status_chk.CheckedChange += (sender1, e1) => ambient_light_sensor_status_spinner.Enabled = e1.IsChecked;

                if (viHeadLampStatus != null)
                {
                    low_beams_on.Checked = viHeadLampStatus.lowBeamsOn;
                    high_beams_on.Checked = viHeadLampStatus.highBeamsOn;
                    ambient_light_sensor_status_spinner.SetSelection((int)viHeadLampStatus.getAmbientLightSensorStatus());
                }
                else
                {
                    viHeadLampStatus = new HeadLampStatus();
                }

                headLampAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    headLampAlertDialog.Dispose();
                });
                headLampAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viHeadLampStatus.lowBeamsOn = low_beams_on.Checked;
                    viHeadLampStatus.highBeamsOn = high_beams_on.Checked;
                    if (ambient_light_sensor_status_chk.Checked)
                    {
                        viHeadLampStatus.ambientLightSensorStatus = (AmbientLightStatus)ambient_light_sensor_status_spinner.SelectedItemPosition;
                    }
                });
                headLampAlertDialog.Show();
            };

            ecall_info_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder eCallInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View eCallInfoView = inflater.Inflate(Resource.Layout.vi_ecall_info, null);
                eCallInfoAlertDialog.SetView(eCallInfoView);
                eCallInfoAlertDialog.SetTitle("ECall Info");

                CheckBox ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_chk);
                Spinner ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_spinner);

                CheckBox aux_ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_chk);
                Spinner aux_ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_spinner);

                CheckBox ecall_confirmation_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_chk);
                Spinner ecall_confirmation_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_spinner);

                ecall_notification_status_chk.CheckedChange += (sender1, e1) => ecall_notification_status_spinner.Enabled = e1.IsChecked;
                aux_ecall_notification_status_chk.CheckedChange += (sender1, e1) => aux_ecall_notification_status_spinner.Enabled = e1.IsChecked;
                ecall_confirmation_status_chk.CheckedChange += (sender1, e1) => ecall_confirmation_status_spinner.Enabled = e1.IsChecked;

                var vehicleDataNotificationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataNotificationStatusArray);
                ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;
                aux_ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;

                var ECallConfirmationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, ECallConfirmationStatusArray);
                ecall_confirmation_status_spinner.Adapter = ECallConfirmationStatusAdapter;

                if (null != viECallInfo)
                {
                    ecall_notification_status_spinner.SetSelection((int)viECallInfo.getECallNotificationStatus());
                    aux_ecall_notification_status_spinner.SetSelection((int)viECallInfo.getAuxECallNotificationStatus());
                    ecall_confirmation_status_spinner.SetSelection((int)viECallInfo.getECallConfirmationStatus());
                }
                else
                {
                    viECallInfo = new ECallInfo();
                }

                eCallInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    eCallInfoAlertDialog.Dispose();
                });
                eCallInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (ecall_notification_status_chk.Checked)
                    {
                        viECallInfo.eCallNotificationStatus = (VehicleDataNotificationStatus)ecall_notification_status_spinner.SelectedItemPosition;
                    }
                    if (aux_ecall_notification_status_chk.Checked)
                    {
                        viECallInfo.auxECallNotificationStatus = (VehicleDataNotificationStatus)aux_ecall_notification_status_spinner.SelectedItemPosition;
                    }
                    if (ecall_confirmation_status_chk.Checked)
                    {
                        viECallInfo.eCallConfirmationStatus = (ECallConfirmationStatus)ecall_confirmation_status_spinner.SelectedItemPosition;
                    }
                });
                eCallInfoAlertDialog.Show();
            };

            airbag_status_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder airbagStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View airbagStatusView = inflater.Inflate(Resource.Layout.vi_airbag_status, null);
                airbagStatusAlertDialog.SetView(airbagStatusView);
                airbagStatusAlertDialog.SetTitle("Airbag Status");

                CheckBox driver_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_chk);
                Spinner driver_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_spinner);

                CheckBox driver_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_chk);
                Spinner driver_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_spinner);

                CheckBox driver_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_chk);
                Spinner driver_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_spinner);

                CheckBox passenger_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_chk);
                Spinner passenger_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_spinner);

                CheckBox passenger_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_chk);
                Spinner passenger_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_spinner);

                CheckBox driver_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_chk);
                Spinner driver_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_spinner);

                CheckBox passenger_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_chk);
                Spinner passenger_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_spinner);

                CheckBox passenger_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_chk);
                Spinner passenger_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_spinner);

                driver_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;

                driver_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;

                if (null != viAirbagStatus)
                {
                    driver_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverAirbagDeployed());
                    driver_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverSideAirbagDeployed());
                    driver_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverCurtainAirbagDeployed());
                    passenger_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerAirbagDeployed());
                    passenger_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerCurtainAirbagDeployed());
                    driver_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverKneeAirbagDeployed());
                    passenger_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerSideAirbagDeployed());
                    passenger_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerKneeAirbagDeployed());
                }
                else
                {
                    viAirbagStatus = new AirbagStatus();
                }

                airbagStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    airbagStatusAlertDialog.Dispose();
                });
                airbagStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (driver_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverAirbagDeployed = (VehicleDataEventStatus)driver_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_side_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverSideAirbagDeployed = (VehicleDataEventStatus)driver_side_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_curtain_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverCurtainAirbagDeployed = (VehicleDataEventStatus)driver_curtain_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerAirbagDeployed = (VehicleDataEventStatus)passenger_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_curtain_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerCurtainAirbagDeployed = (VehicleDataEventStatus)passenger_curtain_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_knee_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverKneeAirbagDeployed = (VehicleDataEventStatus)driver_knee_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_side_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerSideAirbagDeployed = (VehicleDataEventStatus)passenger_side_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_knee_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerKneeAirbagDeployed = (VehicleDataEventStatus)passenger_knee_airbag_deployed_spinner.SelectedItemPosition;
                    }
                });
                airbagStatusAlertDialog.Show();
            };

            emergency_event_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder emergencyEventAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View emergencyEventView = inflater.Inflate(Resource.Layout.vi_emergency_event, null);
                emergencyEventAlertDialog.SetView(emergencyEventView);
                emergencyEventAlertDialog.SetTitle("Emergency Event");

                CheckBox emergency_event_type_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_chk);
                Spinner emergency_event_type_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_spinner);

                CheckBox fuel_cutoff_status_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_chk);
                Spinner fuel_cutoff_status_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_spinner);

                CheckBox rollover_event_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_chk);
                Spinner rollover_event_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_spinner);

                CheckBox maximum_change_velocity_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_chk);
                Spinner maximum_change_velocity_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_spinner);

                CheckBox multiple_events_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_chk);
                Spinner multiple_events_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_spinner);

                var EmergencyEventTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, EmergencyEventTypeArray);
                emergency_event_type_spinner.Adapter = EmergencyEventTypeAdapter;

                var FuelCutoffStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, FuelCutoffStatusArray);
                fuel_cutoff_status_spinner.Adapter = FuelCutoffStatusAdapter;

                rollover_event_spinner.Adapter = VehicleDataEventStatusAdapter;
                maximum_change_velocity_spinner.Adapter = VehicleDataEventStatusAdapter;
                multiple_events_spinner.Adapter = VehicleDataEventStatusAdapter;

                if (null != viEmergencyEvent)
                {
                    emergency_event_type_spinner.SetSelection((int)viEmergencyEvent.getEmergencyEventType());
                    fuel_cutoff_status_spinner.SetSelection((int)viEmergencyEvent.getFuelCutoffStatus());
                    rollover_event_spinner.SetSelection((int)viEmergencyEvent.getRolloverEvent());
                    maximum_change_velocity_spinner.SetSelection((int)viEmergencyEvent.getMaximumChangeVelocity());
                    multiple_events_spinner.SetSelection((int)viEmergencyEvent.getMultipleEvents());
                }
                else
                {
                    viEmergencyEvent = new EmergencyEvent();
                }

                emergency_event_type_chk.CheckedChange += (sender1, e1) => emergency_event_type_spinner.Enabled = e1.IsChecked;
                fuel_cutoff_status_chk.CheckedChange += (sender1, e1) => fuel_cutoff_status_spinner.Enabled = e1.IsChecked;
                rollover_event_chk.CheckedChange += (sender1, e1) => rollover_event_spinner.Enabled = e1.IsChecked;
                maximum_change_velocity_chk.CheckedChange += (sender1, e1) => maximum_change_velocity_spinner.Enabled = e1.IsChecked;
                multiple_events_chk.CheckedChange += (sender1, e1) => multiple_events_spinner.Enabled = e1.IsChecked;

                emergencyEventAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    emergencyEventAlertDialog.Dispose();
                });
                emergencyEventAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    if (emergency_event_type_chk.Checked)
                    {
                        viEmergencyEvent.emergencyEventType = (EmergencyEventType)emergency_event_type_spinner.SelectedItemPosition;
                    }
                    if (fuel_cutoff_status_chk.Checked)
                    {
                        viEmergencyEvent.fuelCutoffStatus = (FuelCutoffStatus)fuel_cutoff_status_spinner.SelectedItemPosition;
                    }
                    if (rollover_event_chk.Checked)
                    {
                        viEmergencyEvent.rolloverEvent = (VehicleDataEventStatus)rollover_event_spinner.SelectedItemPosition;
                    }
                    if (maximum_change_velocity_chk.Checked)
                    {
                        viEmergencyEvent.maximumChangeVelocity = (VehicleDataEventStatus)maximum_change_velocity_spinner.SelectedItemPosition;
                    }
                    if (multiple_events_chk.Checked)
                    {
                        viEmergencyEvent.multipleEvents = (VehicleDataEventStatus)multiple_events_spinner.SelectedItemPosition;
                    }
                });
                emergencyEventAlertDialog.Show();
            };

            cluster_modes_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder clusterModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View clusterModeView = inflater.Inflate(Resource.Layout.vi_cluster_mode_status, null);
                clusterModeAlertDialog.SetView(clusterModeView);
                clusterModeAlertDialog.SetTitle("Cluster Mode Status");

                CheckBox power_mode_active = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active);

                CheckBox power_mode_qualification_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_chk);
                Spinner power_mode_qualification_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_spinner);

                CheckBox car_mode_status_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_chk);
                Spinner car_mode_status_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_spinner);

                CheckBox power_mode_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_chk);
                Spinner power_mode_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_spinner);

                var PowerModeQualificationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeQualificationStatusArray);
                power_mode_qualification_status_spinner.Adapter = PowerModeQualificationStatusAdapter;

                var CarModeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, CarModeStatusArray);
                car_mode_status_status_spinner.Adapter = CarModeStatusAdapter;

                var PowerModeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeStatusArray);
                power_mode_status_spinner.Adapter = PowerModeStatusAdapter;

                if (null != viClusterModeStatus)
                {
                    power_mode_active.Checked = viClusterModeStatus.powerModeActive;
                    power_mode_qualification_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeQualificationStatus());
                    car_mode_status_status_spinner.SetSelection((int)viClusterModeStatus.getCarModeStatus());
                    power_mode_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeStatus());
                }
                else
                {
                    viClusterModeStatus = new ClusterModeStatus();
                }

                power_mode_qualification_status_chk.CheckedChange += (sender1, e1) => power_mode_qualification_status_spinner.Enabled = e1.IsChecked;
                car_mode_status_status_chk.CheckedChange += (sender1, e1) => car_mode_status_status_spinner.Enabled = e1.IsChecked;
                power_mode_status_chk.CheckedChange += (sender1, e1) => power_mode_status_spinner.Enabled = e1.IsChecked;

                clusterModeAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    clusterModeAlertDialog.Dispose();
                });
                clusterModeAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viClusterModeStatus.powerModeActive = power_mode_active.Checked;
                    if (power_mode_qualification_status_chk.Checked)
                    {
                        viClusterModeStatus.powerModeQualificationStatus = (PowerModeQualificationStatus)power_mode_qualification_status_spinner.SelectedItemPosition;
                    }
                    if (car_mode_status_status_chk.Checked)
                    {
                        viClusterModeStatus.carModeStatus = (CarModeStatus)car_mode_status_status_spinner.SelectedItemPosition;
                    }
                    if (power_mode_qualification_status_chk.Checked)
                    {
                        viClusterModeStatus.powerModeStatus = (PowerModeStatus)power_mode_status_spinner.SelectedItemPosition;

					}
				});
				clusterModeAlertDialog.Show();
			};

            fuel_range_btn.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder fuelRangeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View fuelRangeView = inflater.Inflate(Resource.Layout.add_fuel_range, null);
                fuelRangeAlertDialog.SetView(fuelRangeView);
                fuelRangeAlertDialog.SetTitle("Add Fuel Range");

                CheckBox checkBoxName = (CheckBox)fuelRangeView.FindViewById(Resource.Id.fuel_type_check);
                Spinner spnName = (Spinner)fuelRangeView.FindViewById(Resource.Id.fuel_type_spinner);

                string[] fuelTypeNames = Enum.GetNames(typeof(FuelType));
                var fuelTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, fuelTypeNames);
                spnName.Adapter = fuelTypeAdapter;

                CheckBox rangeChk = (CheckBox)fuelRangeView.FindViewById(Resource.Id.range_check);
                EditText rangeET = (EditText)fuelRangeView.FindViewById(Resource.Id.range_et);

                checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                rangeChk.CheckedChange += (sender, e) => rangeET.Enabled = e.IsChecked;

                fuelRangeAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    FuelRange fuelRange = new FuelRange();

                    if (checkBoxName.Checked)
                    {
                        fuelRange.type = (FuelType)spnName.SelectedItemPosition;
                    }
                    if (rangeChk.Checked)
                    {
                        try
                        {
                            fuelRange.range = Java.Lang.Float.ParseFloat(rangeET.Text);
                        }
                        catch (Exception e)
                        {
                            fuelRange.range = 0;
                        }
                    }

                    fuelRangeList.Add(fuelRange);
                    fuelRangeAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(fuel_range_lv);
                });

                fuelRangeAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    fuelRangeAlertDialog.Dispose();
                });
                fuelRangeAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_chk.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;
                }

                GPSData gpsdata = null;
                if (gps_data_chk.Checked)
                {
                    gpsdata = viGPSData;
                }

                float? speed = null;
                if (speed_chk.Checked && speed_et.Text != null && speed_et.Text.Length > 0)
                {
                    speed = Java.Lang.Float.ParseFloat(speed_et.Text);
                }

                int? rpm = null;
                if (rpm_chk.Checked && rpm_et.Text != null && rpm_et.Text.Length > 0)
                {
                    rpm = Java.Lang.Integer.ParseInt(rpm_et.Text);
                }

                float? fuelLevel = null;
                if (fuel_level_chk.Checked && fuel_level_et.Text != null && fuel_level_et.Text.Length > 0)
                {
                    fuelLevel = Java.Lang.Float.ParseFloat(fuel_level_et.Text);
                }

                ComponentVolumeStatus? fuelLevel_State = null;
                if (fuel_level_state_chk.Checked)
                {
                    fuelLevel_State = (ComponentVolumeStatus)fuel_level_state_spinner.SelectedItemPosition;
                }

                float? instantFuelConsumption = null;
                if (instant_fuel_consumption_chk.Checked && instant_fuel_consumption_et.Text != null && instant_fuel_consumption_et.Text.Length > 0)
                {
                    instantFuelConsumption = Java.Lang.Float.ParseFloat(instant_fuel_consumption_et.Text);
                }

                float? externalTemperature = null;
                if (external_temp_chk.Checked && external_temp_et.Text != null && external_temp_et.Text.Length > 0)
                {
                    externalTemperature = Java.Lang.Float.ParseFloat(external_temp_et.Text);
                }

                string vin = null;
                if (vin_chk.Checked)
                {
                    vin = vin_et.Text;
                }

                PRNDL? prndl = null;
                if (prndl_chk.Checked)
                {
                    prndl = (PRNDL)prndl_spinner.SelectedItemPosition;
                }

                TireStatus tirePressure = null;
                if (tire_pressure_chk.Checked)
                {
                    tirePressure = viTireStatus;
                }

                int? odometer = null;
                if (odometer_chk.Checked && odometer_et.Text != null && odometer_et.Text.Length > 0)
                {
                    odometer = Java.Lang.Integer.ParseInt(odometer_et.Text);
                }

                BeltStatus beltStatus = null;
                if (belt_status_chk.Checked)
                {
                    beltStatus = viBeltStatus;
                }

                BodyInformation bodyInformation = null;
                if (body_info_chk.Checked)
                {
                    bodyInformation = viBodyInformation;
                }

                DeviceStatus deviceStatus = null;
                if (device_status_chk.Checked)
                {
                    deviceStatus = viDeviceStatus;
                }

                VehicleDataEventStatus? driverBraking = null;
                if (driver_braking_chk.Checked)
                {
                    driverBraking = (VehicleDataEventStatus)driver_braking_spinner.SelectedItemPosition;
                }

                WiperStatus? wiperStatus = null;
                if (wiper_status_chk.Checked)
                {
                    wiperStatus = (WiperStatus)wiper_status_spinner.SelectedItemPosition;
                }

                HeadLampStatus headLampStatus = null;
                if (head_lamp_status_chk.Checked)
                {
                    headLampStatus = viHeadLampStatus;
                }

                float? engineTorque = null;
                if (engine_torque_chk.Checked && engine_torque_et.Text != null && engine_torque_et.Text.Length > 0)
                {
                    engineTorque = Java.Lang.Float.ParseFloat(engine_torque_et.Text);
                }

                float? accPedalPosition = null;
                if (acc_padel_position_chk.Checked && acc_padel_position_et.Text != null && acc_padel_position_et.Text.Length > 0)
                {
                    accPedalPosition = Java.Lang.Float.ParseFloat(acc_padel_position_et.Text);
                }

                float? steeringWheelAngle = null;
                if (steering_wheel_angle_chk.Checked && steering_wheel_angle_et.Text != null && steering_wheel_angle_et.Text.Length > 0)
                {
                    steeringWheelAngle = Java.Lang.Float.ParseFloat(steering_wheel_angle_et.Text);
                }

                ECallInfo eCallInfo = null;
                if (ecall_info_chk.Checked)
                {
                    eCallInfo = viECallInfo;
                }

                AirbagStatus airbagStatus = null;
                if (airbag_status_chk.Checked)
                {
                    airbagStatus = viAirbagStatus;
                }

                EmergencyEvent emergencyEvent = null;
                if (emergency_event_chk.Checked)
                {
                    emergencyEvent = viEmergencyEvent;
                }

                ClusterModeStatus clusterModeStatus = null;
                if (cluster_modes_chk.Checked)
                {
                    clusterModeStatus = viClusterModeStatus;
                }

                MyKey myKey = null;
                if (my_key_chk.Checked)
                {
                    myKey = new MyKey();
                    myKey.e911Override = (VehicleDataStatus)my_key_spinner.SelectedItemPosition;
                }

                ElectronicParkBrakeStatus? electronicParkBrakeStatus = null;
                if (electronic_park_brake_status_chk.Checked)
                {
                    electronicParkBrakeStatus = (ElectronicParkBrakeStatus)electronic_park_brake_status_spinner.SelectedItemPosition;
                }

                float? engineOilLife = null;
                if (engine_oil_life_chk.Checked)
                {
                    try
                    {
                        engineOilLife = Java.Lang.Float.ParseFloat(engine_oil_life_et.Text);
                    }
                    catch (Exception e)
                    {
                        engineOilLife = null;
                    }
                }

                List<FuelRange> fuelRange = null;
                if (fuel_range_chk.Checked)
                {
                    fuelRange = fuelRangeList;
                }

				RpcResponse rpcMessage = BuildRpc.buildVehicleInfoGetVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gpsdata, speed, rpm, fuelLevel,
                                                                                         fuelLevel_State, instantFuelConsumption, externalTemperature, vin,
                                                                                        prndl, tirePressure, odometer, beltStatus, bodyInformation,
                                                                                        deviceStatus, driverBraking, wiperStatus, headLampStatus,
                                                                                        engineTorque, accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus,
                                                                                        emergencyEvent, clusterModeStatus, myKey, electronicParkBrakeStatus, engineOilLife, fuelRange);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void dialogClosable(DialogInterface dialogInterface, Boolean close)
        {
            try
            {
                Java.Lang.Reflect.Field field = dialogInterface.Class.Superclass.GetDeclaredField("mShowing");
                field.Accessible = true;
                field.Set(dialogInterface, close);
            }
            catch (Exception e)
            {

            }
        }

        protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switch (requestCode)
            {
                case 45:
                    //int REQUEST_FILE_CHOOSER = 45;
                    if (resultCode == Android.App.Result.Ok)
                    {
                        String localFilePath = (String)IntentHelper.
                                getObjectForKey(Const.INTENTHELPER_KEY_FILECHOOSER_FILE);
                        if (localFilePath != null)
                        {
                            txtLocalFileName.SetText(localFilePath, TextView.BufferType.Editable);
                        }
                    }
                    IntentHelper.removeObjectForKey(Const.INTENTHELPER_KEY_FILECHOOSER_FILE);
                    break;
            }
        }

        public void CreateRCResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.rc_get_capabilities, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("RC.GetCapabilities");

            CheckBox remoteControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_remote_control_capabilities_cb);

            CheckBox climateControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_cb);
            Button addClimateControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_btn);
            ListView climateControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_lv);
            List<ClimateControlCapabilities> climateControlCapabilitiesList = new List<ClimateControlCapabilities>();

            CheckBox radioControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_cb);
            Button addRadioControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_btn);
            ListView radioControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_lv);
            List<RadioControlCapabilities> radioControlCapabilitiesList = new List<RadioControlCapabilities>();

            CheckBox buttonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_button_capabilities_cb);
            Button addButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_button_capabilities_btn);
            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_button_capabilities_lv);
            List<ButtonCapabilities> btnCapList = new List<ButtonCapabilities>();

            CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.rc_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.rc_result_code_spn);

            var climateControlCapabilitiesAdapter = new ClimateControlCapabilitiesAdapter(this, climateControlCapabilitiesList);
            climateControlCapabilitiesListView.Adapter = climateControlCapabilitiesAdapter;

            var radioControlCapabilitiesAdapter = new RadioControlCapabilitiesAdapter(this, radioControlCapabilitiesList);
            radioControlCapabilitiesListView.Adapter = radioControlCapabilitiesAdapter;

            var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, btnCapList);
            buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;

            resultCodeSpn.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);

            HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            String climateModuleName = null;
            String radioModuleName = null;

            if (tmpObj != null && tmpObj.getRemoteControlCapabilities() != null)
            {
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

                if (tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities() != null)
                {
                    climateControlCapabilitiesList.AddRange(tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities());
                    if (tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities().Count > 0)
                    {
                        climateModuleName = tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities()[0].getModuleName();
                    }
                    climateControlCapabilitiesAdapter.NotifyDataSetChanged();
                }

                if (tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities() != null)
                {
                    radioControlCapabilitiesList.AddRange(tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities());
                    if (tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities().Count > 0)
                    {
                        radioModuleName = tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities()[0].getModuleName();
                    }
                    radioControlCapabilitiesAdapter.NotifyDataSetChanged();
                }

                if (tmpObj.getRemoteControlCapabilities().getButtonCapabilities() != null)
                {
                    btnCapList.AddRange(tmpObj.getRemoteControlCapabilities().getButtonCapabilities());
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                }
            }
            Utility.setListViewHeightBasedOnChildren(climateControlCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(radioControlCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);

            remoteControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                climateControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
                climateControlCapabilitiesCheckBox.Checked = evn.IsChecked;

                radioControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
                radioControlCapabilitiesCheckBox.Checked = evn.IsChecked;

                buttonCapabilitiesCheckBox.Enabled = evn.IsChecked;
                buttonCapabilitiesCheckBox.Checked = evn.IsChecked;
            };

            climateControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addClimateControlCapabilitiesButton.Enabled = evn.IsChecked;
                climateControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            radioControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addRadioControlCapabilitiesButton.Enabled = evn.IsChecked;
                radioControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            buttonCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addButtonCapabilitiesButton.Enabled = evn.IsChecked;
                buttonCapabilitiesListView.Enabled = evn.IsChecked;
            };
            resultCodeCb.CheckedChange += (sen, evn) => resultCodeSpn.Enabled = evn.IsChecked;

            addClimateControlCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder climateControlCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View climateControlCapabilitiesView = inflater.Inflate(Resource.Layout.climate_control_capabilities, null);
                climateControlCapabilitiesAlertDialog.SetView(climateControlCapabilitiesView);
                climateControlCapabilitiesAlertDialog.SetTitle("ClimateControlCapabilities");

                CheckBox moduleNameCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_module_name_cb);
                EditText moduleNameEt = (EditText)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_module_name_et);
                if (climateModuleName != null) moduleNameEt.Text = climateModuleName;
                CheckBox currentTempAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_current_temp_available_cb);
                CheckBox fanSpeedAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_fan_speed_available_cb);
                CheckBox desiredTemperatureAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_desired_temperature_available_cb);
                CheckBox acEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_enable_available_cb);
                CheckBox acMaxEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_max_enable_available_cb);
                CheckBox circulateAirEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_circulate_air_enable_available_cb);
                CheckBox autoModeEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_auto_mode_enable_available_cb);
                CheckBox dualModeEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_dual_mode_enable_available_cb);
                CheckBox defrostZoneAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_available_cb);

                CheckBox defrostZoneCheckBox = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_cb);
                Button addDefrostZoneButton = (Button)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_btn);
                ListView defrostZoneListView = (ListView)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_lv);
                List<DefrostZone> defrostZoneList = new List<DefrostZone>();

                CheckBox ventilationModeAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_available_cb);

                CheckBox ventilationModeCheckBox = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_cb);
                Button addVentilationModeButton = (Button)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_btn);
                ListView ventilationModeListView = (ListView)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_lv);
                List<VentilationMode> ventilationModeList = new List<VentilationMode>();

                moduleNameCb.CheckedChange += (sen, evn) => moduleNameEt.Enabled = evn.IsChecked;
                defrostZoneCheckBox.CheckedChange += (sen, evn) =>
                {
                    addDefrostZoneButton.Enabled = evn.IsChecked;
                    defrostZoneListView.Enabled = evn.IsChecked;
                };

                ventilationModeCheckBox.CheckedChange += (sen, evn) =>
                {
                    addVentilationModeButton.Enabled = evn.IsChecked;
                    ventilationModeListView.Enabled = evn.IsChecked;
                };


                var defrostZoneAdapter = new DefrostZoneAdapter(this, defrostZoneList);
                defrostZoneListView.Adapter = defrostZoneAdapter;

                var ventilationModeAdapter = new VentilationModeAdapter(this, ventilationModeList);
                ventilationModeListView.Adapter = ventilationModeAdapter;

                bool[] defrostZoneBoolArray = new bool[RCDefrostZoneArray.Length];
                addDefrostZoneButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder defrostZoneAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    defrostZoneAlertDialog.SetTitle("DefrostZone");

                    defrostZoneAlertDialog.SetMultiChoiceItems(RCDefrostZoneArray, defrostZoneBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => defrostZoneBoolArray[e.Which] = e.IsChecked);

                    defrostZoneAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        defrostZoneAlertDialog.Dispose();
                    });

                    defrostZoneAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
                        if (defrostZoneCheckBox.Checked)
                        {
                            defrostZoneList.Clear();

                            for (int i = 0; i < RCDefrostZoneArray.Length; i++)
                            {
                                if (defrostZoneBoolArray[i])
                                {
                                    defrostZoneList.Add(((DefrostZone)typeof(DefrostZone).GetEnumValues().GetValue(i)));
                                }
                            }
                            defrostZoneAdapter.NotifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(defrostZoneListView);
                        }
                    });
                    defrostZoneAlertDialog.Show();
                };

                bool[] ventilationModeBoolArray = new bool[RCVentilationModeArray.Length];
                addVentilationModeButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder ventilationModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    ventilationModeAlertDialog.SetTitle("VentilationMode");

                    ventilationModeAlertDialog.SetMultiChoiceItems(RCVentilationModeArray, ventilationModeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => ventilationModeBoolArray[e.Which] = e.IsChecked);

                    ventilationModeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        ventilationModeAlertDialog.Dispose();
                    });

                    ventilationModeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
                        if (ventilationModeCheckBox.Checked)
                        {
                            ventilationModeList.Clear();
                            for (int i = 0; i < RCVentilationModeArray.Length; i++)
                            {
                                if (ventilationModeBoolArray[i])
                                {
                                    ventilationModeList.Add(((VentilationMode)typeof(VentilationMode).GetEnumValues().GetValue(i)));
                                }
                            }
                            ventilationModeAdapter.NotifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(ventilationModeListView);
                        }
                    });
                    ventilationModeAlertDialog.Show();
                };


                climateControlCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    ClimateControlCapabilities climateControlCapabilities = new ClimateControlCapabilities();
                    if (moduleNameCb.Checked)
                        climateControlCapabilities.moduleName = moduleNameEt.Text;
                    else
                        climateControlCapabilities.moduleName = "";

                    climateControlCapabilities.currentTemperatureAvailable = currentTempAvailableCb.Checked;
                    climateControlCapabilities.fanSpeedAvailable = fanSpeedAvailableCb.Checked;
                    climateControlCapabilities.desiredTemperatureAvailable = desiredTemperatureAvailableCb.Checked;
                    climateControlCapabilities.acEnableAvailable = acEnableAvailableCb.Checked;
                    climateControlCapabilities.acMaxEnableAvailable = acMaxEnableAvailableCb.Checked;
                    climateControlCapabilities.circulateAirEnableAvailable = circulateAirEnableAvailableCb.Checked;
                    climateControlCapabilities.autoModeEnableAvailable = autoModeEnableAvailableCb.Checked;
                    climateControlCapabilities.dualModeEnableAvailable = dualModeEnableAvailableCb.Checked;
                    climateControlCapabilities.defrostZoneAvailable = defrostZoneAvailableCb.Checked;

                    if (defrostZoneCheckBox.Checked)
                        climateControlCapabilities.defrostZone = defrostZoneList;

                    climateControlCapabilities.ventilationModeAvailable = ventilationModeAvailableCb.Checked;

                    if (ventilationModeCheckBox.Checked)
                        climateControlCapabilities.ventilationMode = ventilationModeList;

                    climateControlCapabilitiesList.Add(climateControlCapabilities);
                    climateControlCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(climateControlCapabilitiesListView);
                });

                climateControlCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    climateControlCapabilitiesAlertDialog.Dispose();
                });
                climateControlCapabilitiesAlertDialog.Show();
            };

            addRadioControlCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder radioControlCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View radioControlCapabilitiesView = inflater.Inflate(Resource.Layout.radio_control_capabilities, null);
                radioControlCapabilitiesAlertDialog.SetView(radioControlCapabilitiesView);
                radioControlCapabilitiesAlertDialog.SetTitle("RadioControlCapabilities");

                CheckBox moduleNameCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_module_name_cb);
                EditText moduleNameEt = (EditText)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_module_name_et);
                if (radioModuleName != null) moduleNameEt.Text = radioModuleName;
                CheckBox radioEnableAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_enable_available_cb);
                CheckBox radioBandAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_band_available_cb);
                CheckBox radioFrequencyAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_frequency_available_cb);
                CheckBox hdChannelAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_hd_channel_available_cb);
                CheckBox rdsDataAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_rds_data_available_cb);
                CheckBox availableHDsAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_available_hds_available_cb);
                CheckBox stateAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_state_available_cb);
                CheckBox signalStrengthAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_strength_available_cb);
                CheckBox signalChangeThresholdAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_available_cb);

                moduleNameCb.CheckedChange += (sen, evn) => moduleNameEt.Enabled = evn.IsChecked;

                radioControlCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    RadioControlCapabilities radioControlCapabilities = new RadioControlCapabilities();

                    if (moduleNameCb.Checked)
                        radioControlCapabilities.moduleName = moduleNameEt.Text;
                    else
                        radioControlCapabilities.moduleName = "";

                    radioControlCapabilities.radioEnableAvailable = radioEnableAvailableCb.Checked;
                    radioControlCapabilities.radioBandAvailable = radioBandAvailableCb.Checked;
                    radioControlCapabilities.radioFrequencyAvailable = radioFrequencyAvailableCb.Checked;
                    radioControlCapabilities.hdChannelAvailable = hdChannelAvailableCb.Checked;
                    radioControlCapabilities.rdsDataAvailable = rdsDataAvailableCb.Checked;
                    radioControlCapabilities.availableHDsAvailable = availableHDsAvailableCb.Checked;
                    radioControlCapabilities.stateAvailable = stateAvailableCb.Checked;
                    radioControlCapabilities.signalStrengthAvailable = signalStrengthAvailableCb.Checked;
                    radioControlCapabilities.signalChangeThresholdAvailable = signalChangeThresholdAvailableCb.Checked;

                    radioControlCapabilitiesList.Add(radioControlCapabilities);
                    radioControlCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(radioControlCapabilitiesListView);
                });

                radioControlCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    radioControlCapabilitiesAlertDialog.Dispose();
                });
                radioControlCapabilitiesAlertDialog.Show();
            };

            addButtonCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder btnCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View btnCapabilitiesView = inflater.Inflate(Resource.Layout.button_capabilities, null);
                btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
                btnCapabilitiesAlertDialog.SetTitle("ButtonCapabilities");

                TextView textViewButtonName = (TextView)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);

                Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);
                string[] btnCapabilitiesButtonName = Enum.GetNames(typeof(ButtonName));
                var btnCapabilitiesButtonNameAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, btnCapabilitiesButtonName);
                spnButtonNames.Adapter = btnCapabilitiesButtonNameAdapter;

                CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);
                CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);
                CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);

                btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    ButtonCapabilities btn = new ButtonCapabilities();
                    btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;
                    btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
                    btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
                    btn.upDownAvailable = checkBoxUpDownAvailable.Checked;

                    btnCapList.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);
                });

                btnCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    btnCapabilitiesAlertDialog.Dispose();
                });
                btnCapabilitiesAlertDialog.Show();
            };


            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, evn) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, evn) =>
            {
                RemoteControlCapabilities remoteControlCapabilities = new RemoteControlCapabilities();

                if (remoteControlCapabilitiesCheckBox.Checked)
                {
                    if (climateControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.climateControlCapabilities = climateControlCapabilitiesList;

                    if (radioControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.radioControlCapabilities = radioControlCapabilitiesList;

                    if (buttonCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.buttonCapabilities = btnCapList;
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCb.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildRcGetCapabilitiesResponse(BuildRpc.getNextId(), rsltCode, remoteControlCapabilities);
                AppUtils.savePreferenceValueForRpc(this, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, even) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateRCResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("RC.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            HmiApiLib.Controllers.RC.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj != null)
            {
                checkBoxAvailable.Checked = (bool)tmpObj.getAvailable();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            resultCodeCb.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("TX Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (resultCodeCb.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.RC, checkBoxAvailable.Checked, selectedResultCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }
    }
}