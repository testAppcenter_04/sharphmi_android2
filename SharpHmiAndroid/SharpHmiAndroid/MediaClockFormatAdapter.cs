﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
	public class MediaClockFormatAdapter : BaseAdapter<MediaClockFormat>
	{
        List<MediaClockFormat> mediaClockFormatList;
		Activity context;

		public MediaClockFormatAdapter(Activity act, List<MediaClockFormat> list) : base()
		{
			mediaClockFormatList = list;
			context = act;
		}

		public override MediaClockFormat this[int position] => mediaClockFormatList[position];

		public override int Count => mediaClockFormatList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = mediaClockFormatList[position].ToString();

			return view;
		}
	}
}
