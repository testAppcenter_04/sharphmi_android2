﻿using System;
using System.Collections.Generic;

namespace SharpHmiAndroid
{
    public class IntentHelper
    {
		private static IntentHelper _instance;
		private Dictionary<String, Object> _map;

		private IntentHelper()
		{
			_map = new Dictionary<String, Object>();
		}

		private static IntentHelper getInstance()
		{
			if (_instance == null)
			{
				_instance = new IntentHelper();
			}
			return _instance;
		}

		public static void addObjectForKey(Object obj, String key)
		{
			getInstance()._map.Add(key, obj);
		}

		public static Object getObjectForKey(String key)
		{
			return getInstance()._map[key];
		}

		public static void removeObjectForKey(String key)
		{
			getInstance()._map.Remove(key);
		}
    }
}
