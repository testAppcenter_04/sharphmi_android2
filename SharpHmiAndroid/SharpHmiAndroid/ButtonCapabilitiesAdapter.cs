﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
	public class ButtonCapabilitiesAdapter : BaseAdapter<ButtonCapabilities>
	{
		List<ButtonCapabilities> buttonCapabilities;
		Activity context;

		public ButtonCapabilitiesAdapter(Activity act, List<ButtonCapabilities> list) : base()
		{
			buttonCapabilities = list;
			context = act;
		}

		public override ButtonCapabilities this[int position] => buttonCapabilities[position];

		public override int Count => buttonCapabilities.Count;

		public override long GetItemId(int position)
		{
            return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = buttonCapabilities[position].getName().ToString();

			return view;
		}
	}
}
