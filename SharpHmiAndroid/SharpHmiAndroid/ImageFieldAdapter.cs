﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    public class ImageFieldAdapter : BaseAdapter<ImageField>
	{
        List<ImageField> imageFieldList;
		Activity context;

		public ImageFieldAdapter(Activity act, List<ImageField> list) : base()
		{
			imageFieldList = list;
			context = act;
		}

		public override ImageField this[int position] => imageFieldList[position];

		public override int Count => imageFieldList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = imageFieldList[position].getName().ToString();

			return view;
		}
	}
}
